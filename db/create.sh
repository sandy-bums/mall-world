#!/usr/bin/env bash

function createdb() {
    DATABASE=$1
    PGPASSWORD=postgres psql -U mallworlduser -d postgres -h localhost -c "drop database ${DATABASE}"
    PGPASSWORD=postgres psql -U mallworlduser -d postgres -h localhost -c "create database ${DATABASE}"
}

createdb "mallworld"