ALTER TABLE event ADD category VARCHAR(255);
ALTER TABLE event ADD start_time_date TIMESTAMPTZ;
ALTER TABLE event ADD end_time_date TIMESTAMPTZ;
ALTER TABLE event ADD description TEXT;
ALTER TABLE event ADD owner VARCHAR(255);
ALTER TABLE event ADD special_instructions VARCHAR(255);