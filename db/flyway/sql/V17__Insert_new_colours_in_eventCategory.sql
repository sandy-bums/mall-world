UPDATE eventCategory SET colour = '#fff277' WHERE id = 1;
UPDATE eventCategory SET colour = '#ffb993' WHERE id = 2;
UPDATE eventCategory SET colour = '#d9ff84' WHERE id = 3;
UPDATE eventCategory SET colour = '#88d8ff' WHERE id = 4;
UPDATE eventCategory SET colour = '#ffc264' WHERE id = 5;
UPDATE eventCategory SET colour = '#858fdd' WHERE id = 6;
UPDATE eventCategory SET colour = '#b3bafe' WHERE id = 7;