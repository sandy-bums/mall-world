CREATE TABLE UserEvents (
    id BIGSERIAL PRIMARY KEY,
    mallWorldUser BIGINT NOT NULL REFERENCES mallWorldUser(id),
    userEvent BIGINT NOT NULL REFERENCES event(id)
);