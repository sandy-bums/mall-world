CREATE TABLE EstablishmentEvents (
    id BIGSERIAL,
    establishment BIGINT NOT NULL REFERENCES establishment(id),
    event BIGINT NOT NULL REFERENCES event(id),
    PRIMARY KEY(establishment, event)

);