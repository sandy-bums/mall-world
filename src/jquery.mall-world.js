/*
 * mall-world
 * 
 *
 * Copyright (c) 2015 Raluca Puichilita
 * Licensed under the MIT license.
 */

(function($) {

  // Collection method.
  $.fn.mall_world = function() {
    return this.each(function(i) {
      // Do something awesome to each selected element.
      $(this).html('awesome' + i);
    });
  };

  // Static method.
  $.mall_world = function(options) {
    // Override default options with passed-in options.
    options = $.extend({}, $.mall_world.options, options);
    // Return something awesome.
    return 'awesome' + options.punctuation;
  };

  // Static method default options.
  $.mall_world.options = {
    punctuation: '.'
  };

  // Custom selector.
  $.expr[':'].mall_world = function(elem) {
    // Is this element awesome?
    return $(elem).text().indexOf('awesome') !== -1;
  };

}(jQuery));
