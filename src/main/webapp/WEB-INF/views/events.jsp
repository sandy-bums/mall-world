<%@taglib prefix="joda" uri="http://www.joda.org/joda/time/tags" %>

<c:import url="/WEB-INF/views/common/header.jsp">
    <c:param name="title" value="Mall World - My Events"/>
</c:import>

<div class="container" id="wrapper">
    <c:import url="/WEB-INF/views/common/messages.jsp"/>
    <p>
        <c:out value="${deletedEvent}"/>
    </p>

    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <div class="sidebar-nav">
                    <div class="navbar navbar-default" role="navigation">
                        <div class="navbar-header">
                            <button id="sidebarButton" type="button" class="navbar-toggle" data-toggle="collapse"
                                    data-target=".sidebar-navbar-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <span class="visible-xs navbar-brand">Filters</span>
                        </div>
                        <div class="navbar-collapse collapse sidebar-navbar-collapse">
                            <ul class="nav navbar-nav">
                                <li>
                                    <c:import url="/WEB-INF/views/common/sidebar.jsp"/>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-9">
                <c:if test="${not empty sqlError}">
                    <p class="alert alert-danger"><c:out value="${sqlError}"/></p>
                </c:if>

                <c:forEach var="event" items="${events}" varStatus="row">
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="heading${event.id}"
                             style="background:${event.eventCategory.colour}">
                            <h3 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion"
                                   href="#collapse${event.id}" aria-expanded="true"
                                   aria-controls="collapse${event.id}">
                                    <strong><c:out value="${event.title}"/></strong>
                                    @
                                    <c:out value="${event.owner}"/>, from
                                    <joda:format value="${event.startTimeDate}" pattern="dd MMM yyyy HH:mm"
                                                 style="F-"/>
                                    until <joda:format value="${event.endTimeDate}" pattern="dd MMM yyyy HH:mm"
                                                       style="F-"/>
                                </a>

                                <sec:authorize access="authenticated">
                                    <form:form action="/myEvents/addToUser" method="post">
                                        <button type="submit" id="addEventToUser" value="Submit"
                                                class="btn btn-success pull-right"><i class="fa fa-check"></i> I'm going
                                        </button>
                                        <input type="hidden" name="eventId" value="${event.id}"/>
                                    </form:form>
                                </sec:authorize>
                                <span class="clearfix"></span>
                            </h3>
                        </div>
                        <div id="collapse${event.id}" class="panel-collapse collapse"
                             role="tabpanel" aria-labelledby="heading${event.id}">
                            <div class="panel-body">
                                <c:if test="${not empty event.description}">
                                    <h4>Description</h4>

                                    <p><c:out value="${event.description}"/></p>
                                </c:if>

                                <c:if test="${not empty event.eventCategory.name}">
                                    <h4>Category</h4>

                                    <p><c:out value="${event.eventCategory.name}"/></p>
                                </c:if>

                                <c:if test="${not empty event.specialInstructions}">
                                    <h4>Special instructions</h4>

                                    <p><c:out value="${event.specialInstructions}"/></p>
                                </c:if>

                                <c:if test="${not empty establishment.name}">
                                    <h4>Establishment</h4>

                                    <p><c:out value="${establishment.name}"/></p>
                                </c:if>
                            </div>
                        </div>
                    </div>
                </c:forEach>
            </div>
        </div>
    </div>

</div>

<c:import url="/WEB-INF/views/common/footer.jsp"/>
