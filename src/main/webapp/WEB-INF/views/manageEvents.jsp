<%@taglib prefix="joda" uri="http://www.joda.org/joda/time/tags" %>

<c:import url="/WEB-INF/views/common/header.jsp">
    <c:param name="title" value="Mall World - Manage Events"/>
</c:import>


<div class="container">
    <c:if test="${deletedEvent ne null}">
        <p class="alert alert-success">
            <c:out value="${deletedEvent}"/>
        </p>
    </c:if>
    <div class="row">
        <div class="col-sm-3">
            <c:import url="/WEB-INF/views/common/adminSidebar.jsp"/>
        </div>

        <div class="col-sm-9">
            <h3>Manage Events</h3>
            <hr/>
            <c:forEach var="event" items="${ownedEvents}" varStatus="row">

                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="heading${event.id}" style="background:${event.eventCategory.colour}">
                        <h4 class="panel-title">
                        <c:out value="${event.title}"/>
                        --
                        <c:out value="${event.owner}"/>
                        -- <joda:format value="${event.startTimeDate}" pattern="dd-MMM-yyyy HH:mm" style="F-"/>
                        - <joda:format value="${event.endTimeDate}" pattern="dd-MMM-yyyy HH:mm" style="F-"/>

                        <sec:authorize access="hasAnyRole('ROLE_ESTABLISHMENT', 'ROLE_VENUE')">
                        <form:form action="/events/delete" method="post" modelAttribute="id" cssClass="pull-right" >
                            <button type="submit" id="deleteEvent" value="Submit" class="btn btn-default adminButton">Delete</button>
                            <input type="hidden" name ="id" value="${event.id}" />
                        </form:form>
                        </sec:authorize>
                        <sec:authorize access="hasAnyRole('ROLE_ESTABLISHMENT', 'ROLE_VENUE')">
                        <form:form action="/events/edit" method="post" modelAttribute="id" cssClass="pull-right" >
                            <button type="submit" id="editEvent" value="Submit" class="btn btn-default adminButton">Edit</button>
                            <input type="hidden" name ="id" value="${event.id}" />
                        </form:form>
                        </sec:authorize>
                        <span class="clearfix"></span>
                        </h4>
                    </div>
                 </div>
            </c:forEach>
        </div>
    </div>
</div>

<c:import url="/WEB-INF/views/common/footer.jsp"/>
