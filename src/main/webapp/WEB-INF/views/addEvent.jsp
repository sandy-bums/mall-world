<c:import url="/WEB-INF/views/common/header.jsp">
    <c:param name="title" value="Mall World - Add Event"/>
</c:import>

<div class="container" id="wrapper">
    <c:import url="/WEB-INF/views/common/messages.jsp"/>

    <div class="container">
        <div class="col-sm-3">
            <c:import url="/WEB-INF/views/common/adminSidebar.jsp"/>
        </div>

        <div class="col-sm-9">
            <h3> Add Event </h3>
            <hr/>
            <form:form method="post" modelAttribute="event" action="/events/add?${_csrf.parameterName}=${_csrf.token}"
                       cssClass="form-horizontal" enctype="multipart/form-data">
                <div class="form-group">
                    <form:label path="category" cssClass="col-sm-2">Category</form:label>
                    <div class="col-sm-4">
                        <form:select path="category" multiple="true">
                            <c:forEach var="selectedCategory" items="${eventCategories}">
                                <form:option value="${selectedCategory.name}"/>
                            </c:forEach>
                        </form:select>

                        <form:errors path="category" element="div"/>
                    </div>
                </div>

                <div class="form-group">
                    <form:label path="title" cssClass="col-sm-2">Title</form:label>
                    <div class="col-sm-4">
                        <form:input path="title" cssClass="form-control" required="required"/>
                        <form:errors path="title" element="div"/>
                    </div>
                </div>

                <div class="form-group">
                    <form:label path="startDate" cssClass="col-sm-2">Start Date</form:label>
                    <div class="col-sm-4">
                        <form:input path="startDate" cssClass="form-control" type="datetime-local" required="required"/>
                        <form:errors path="startDate" element="div"/>
                    </div>
                </div>

                <div class="form-group">
                    <form:label path="endDate" cssClass="col-sm-2">End Date</form:label>
                    <div class="col-sm-4">
                        <form:input path="endDate" cssClass="form-control" type="datetime-local" required="required"/>
                        <form:errors path="endDate" element="div"/>
                        <form:errors path="startDateBeforeEndDate" element="div"/>
                        <form:errors path="startEndYearValid" element="div"/>
                    </div>
                </div>

                <div class="form-group">
                    <form:label path="description" cssClass="col-sm-2">Description</form:label>
                    <div class="col-sm-4">
                        <form:textarea cssClass="form-control" path="description"/>
                    </div>
                </div>

                <div class="form-group">
                    <form:label path="specialInstructions" cssClass="col-sm-2">Special Instructions</form:label>
                    <div class="col-sm-4">
                        <form:textarea cssClass="form-control" path="specialInstructions"/>
                        <form:errors path="specialInstructions" element="div"/>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-4">
                        <label for="uploadEventImage" cssClass="col-sm-2">Upload image</label>
                        <input id="uploadEventImage" name="file" type="file"/>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-2">
                        <button type="submit" id="saveEvent" value="Submit" class="btn btn-default">Save Event</button>
                    </div>
                </div>
            </form:form>
        </div>
    </div>
</div>

<c:import url="/WEB-INF/views/common/footer.jsp"/>
