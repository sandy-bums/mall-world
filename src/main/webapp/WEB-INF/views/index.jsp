<c:import url="/WEB-INF/views/common/header.jsp">
    <c:param name="title" value="Mall World"/>
</c:import>

<script>
    $('body').addClass('homepage');
</script>>

<div class="main-banner">
    <div class="container">
        <div class="jumbotron">
            <div class="heading">
                <h1>It's a mall world!</h1>
                <p>Mall World connects you and your friends with events of all kinds.</p>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="look-around">
        <div class="col-xs-4">
            <a href="/calendar" id="calendarLink"><span class="fa-stack fa-5x">
                        <i class="fa fa-circle fa-stack-2x circle-icon"></i>
                        <i class="fa fa-calendar fa-stack-1x look-around-icon"></i>
                    </span></a>
            <h3 class="homepage-h3">View calendar</h3>
        </div>

        <div class="col-xs-4">
            <a href="/events" id="eventsLink"><span class="fa-stack fa-5x">
                        <i class="fa fa-circle fa-stack-2x circle-icon"></i>
                        <i class="fa fa-bullhorn fa-stack-1x look-around-icon"></i>
                    </span></a>
            <h3 class="homepage-h3">What&#39;s on</h3>
        </div>

        <div class="col-xs-4">
            <a href="/establishments" id="establishmentsLink"><span class="fa-stack fa-5x">
                        <i class="fa fa-circle fa-stack-2x circle-icon"></i>
                        <i class="fa fa-tags fa-stack-1x look-around-icon"></i>
                    </span></a>
            <h3 class="homepage-h3">See the shops</h3>
        </div>
    </div>
</div>

<c:import url="/WEB-INF/views/common/footer.jsp"/>
