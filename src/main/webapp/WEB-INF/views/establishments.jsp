<c:import url="/WEB-INF/views/common/header.jsp">
    <c:param name="title" value="Mall World - Establishments"/>
</c:import>

<div class="container" id="wrapper">
    <c:import url="/WEB-INF/views/common/messages.jsp"/>
    <c:forEach var="establishment" items="${establishments}" varStatus="row">
     <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="heading${event.id}">
           <h4 class="panel-title">
                <c:out value="${establishment.name}"/>
            </h4>
        </div>
     </div>
    </c:forEach>
</div>

<c:import url="/WEB-INF/views/common/footer.jsp"/>
