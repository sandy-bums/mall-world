<c:import url="/WEB-INF/views/common/header.jsp">
<c:param name="title" value="Mall World - Sign Up"/>
</c:import>

<div class="container" id="wrapper">
    <c:import url="/WEB-INF/views/common/messages.jsp"/>
    <div class="page-header">
        <h1>Join in!</h1>
    </div>

    <form:form action="/signUp" method="post" cssClass="form-horizontal" modelAttribute="user">
        <div class="form-group">
            <label for="email" class="col-sm-2 control-label">Email</label>
            <div class="col-sm-8">
                <form:input path="email" cssClass="form-control"/>
                <form:errors class="form-alert alert alert-danger" path="email" element="div"/>
            </div>
        </div>
        <div class="form-group">
            <label for="password" class="col-sm-2 control-label">Password</label>
            <div class="col-sm-8">
                <form:password path="password" cssClass="form-control"/>
                <form:errors class="form-alert alert alert-danger" path="password" element="div"/>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-8">
                <button type="submit" id="submit" value="Submit" class="btn btn-primary">Sign Up</button>
            </div>
        </div>
    </form:form>
</div>

<c:import url="/WEB-INF/views/common/footer.jsp"/>