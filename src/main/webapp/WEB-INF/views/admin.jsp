<c:import url="/WEB-INF/views/common/header.jsp">
    <c:param name="title" value="Mall World - Admin"/>
</c:import>

<div class="container">
    <div class="row">
        <div class="col-sm-3">
            <c:import url="/WEB-INF/views/common/adminSidebar.jsp"/>
        </div>

        <div class="col-sm-9">

        </div>
    </div>
</div>


<c:import url="/WEB-INF/views/common/footer.jsp"/>
