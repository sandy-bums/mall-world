<c:import url="/WEB-INF/views/common/header.jsp">
    <c:param name="title" value="Mall World - Manage Establishments"/>
</c:import>

<div class="container">
    <div class="row">
        <div class="col-sm-3">
            <c:import url="/WEB-INF/views/common/adminSidebar.jsp"/>
        </div>

        <div class="col-sm-9">
            <h3>Manage Establishments</h3>
            <hr/>
            <c:forEach var="estab" items="${allEstablishments}" varStatus="row">

                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="heading${estab.id}">
                    <h4 class="panel-title">
                    <c:out value="${estab.name}"/>

                    <sec:authorize access="hasAnyRole('ROLE_ESTABLISHMENT', 'ROLE_VENUE')">
                    <form:form action="/establishments/delete" method="post" modelAttribute="id" cssClass="pull-right" >
                        <button type="submit" id="deleteEstab" value="Submit" class="btn btn-default adminButton">Delete</button>
                        <input type="hidden" name="id" value="${estab.id}" />
                    </form:form>
                    </sec:authorize>
                    <sec:authorize access="hasAnyRole('ROLE_ESTABLISHMENT', 'ROLE_VENUE')">
                    <form:form action="/establishments/edit" method="post" modelAttribute="id" cssClass="pull-right" >
                        <button type="submit" id="editEstab" value="Submit" class="btn btn-default adminButton">Edit</button>
                        <input type="hidden" name="id" value="${estab.id}" />
                    </form:form>
                    </sec:authorize>
                    <span class="clearfix"></span>
                    </h4>
                    </div>
                 </div>
            </c:forEach>
        </div>
    </div>
</div>

<c:import url="/WEB-INF/views/common/footer.jsp"/>
