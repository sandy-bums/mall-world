<sec:authorize access="authenticated" var="authenticated"/>
<c:set var="path" value="${requestScope['javax.servlet.forward.servlet_path']}"
       scope="request"/>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>${param.title}</title>
    <meta charset="utf-8">
    <meta name="description" content="Mall World">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel='shortcut icon' href='/resources/img/favicon.ico' type='image/x-icon'/>

    <link rel="stylesheet"
          href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
    <link rel="stylesheet"
          href="<c:url value="/resources/css/calendar.css"/>"/>
    <link rel="stylesheet"
          href="<c:url value="/resources/css/sidebar.css"/>"/>
    <link rel="stylesheet"
          href="<c:url value="/resources/css/adminPages.css"/>"/>
    <link rel="stylesheet"
          href="<c:url value="/resources/css/index.css"/>"/>
    <link rel="stylesheet"
          href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css"/>
    <link rel='stylesheet'
          href='http://fonts.googleapis.com/css?family=Dosis' type='text/css'/>

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
</head>
<body>
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button id="hamburgerButton" type="button" class="navbar-toggle collapsed"
                    data-toggle="collapse"
                    data-target="#navbar-collapse-main">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand navbar-logo" href="/">
                <img src="/resources/img/mall-world-logo-no-background.png"/>
            </a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="navbar-collapse-main">
            <ul class="nav navbar-nav">
                <li class="${ path eq "/calendar" ? "active" : "" }"><a href="/calendar"><i class="fa fa-calendar"></i>
                    Calendar<span class="sr-only">(current)</span></a></li>
                <li class="${ path eq "/events" ? "active" : "" }"><a href="/events"><i class="fa fa-bullhorn"></i>
                    Events</a></li>
                <li class="${ path eq "/establishments" ? "active" : "" }"><a href="/establishments"><i
                        class="fa fa-tags"></i> Establishments</a></li>
                <sec:authorize access="hasAnyRole('ROLE_VENUE')">
                    <li class="${ path eq "/admin" ? "active" : "" }"><a id="adminLink" href="/admin"><i
                            class="fa fa-cogs"></i> Admin</a></li>
                </sec:authorize>
            </ul>

            <form:form action="/events/search" method="get" class="navbar-form navbar-left" role="search">
                <div class="form-group">
                    <input name="search" id="searchText" type="text" class="form-control" placeholder="Search"
                           maxlength="250">
                </div>
                <button type="submit" id="searchSubmit" class="btn btn-default"><i class="fa fa-search"></i></button>
            </form:form>

            <ul class="nav navbar-nav navbar-right">
                <c:if test="${authenticated}">
                    <div class="dropdown user-dropdown">
                        <button class="btn btn-default dropdown-toggle" type="button" id="userDropdown"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            <i class="fa fa-user"></i>
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="userDropdown">
                            <li><a href="#">My account</a></li>
                            <li><a href="/myEvents">My events</a></li>
                            <li role="separator" class="divider"></li>
                            <li><spring:url value="/logout" var="logout"/>
                                <form action="${ logout }" method="post"
                                      class="navbar-form navbar-left">
                                    <sec:csrfInput/>
                                    <button type="submit" id="logout" class="btn btn-danger"><i class="fa fa-sign-out"></i> Log Out</button>
                                </form>
                            </li>
                        </ul>
                    </div>
                </c:if>
                <c:if test="${not authenticated}">
                    <li><a href="/signUp" id="signUp"><i class="fa fa-pencil-square-o"></i> Sign Up</a></li>
                    <li><a href="/logIn" id="logIn"><i class="fa fa-sign-in"></i> Log In</a></li>
                </c:if>
            </ul>

        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container-fluid -->
</nav>
