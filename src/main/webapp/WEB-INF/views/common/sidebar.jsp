
<form:form method="get" modelAttribute="eventFiltersForm" action="/events">

    <c:forEach var="category" items="${eventCategories}">
        <span class="sidebar-categories block" style="background-color:${category.colour}"><form:checkbox id="${category.name}" path="selectedEventCategories" onChange="this.form.submit();" value="${category.name}"/>${category.name}<br></span>
    </c:forEach>

    <div class="form-group">
        <div class="row-sm-2">
            <form:label path="filterStartDate">From</form:label>
        </div>
        <div class="row-sm-2">
          <form:input path="filterStartDate" cssClass="form-control" type="datetime-local" max="9999-01-01T00:00:00"/>
          <form:errors path="filterStartDate" element="div"/>
        </div>
    </div>

    <div class="form-group">
        <div class="row-sm-2">
            <form:label path="filterEndDate">To</form:label>
        </div>
        <div class="row-sm-2">
          <form:input path="filterEndDate" cssClass="form-control" type="datetime-local" max="9999-01-01T00:00:00"/>
          <form:errors path="filterEndDate" element="div"/>
        </div>
    </div>

    <div class="form-group">
        <form:input path="keyword" id="searchText" type="text" class="form-control" placeholder="By Keyword" maxlength="250"/>
    </div>


    <div class="form-group">
        <div class="row-sm-2">
            <button type="submit" id="filterByDate" value="Submit" class="btn btn-default">Filter</button>
        </div>
    </div>
</form:form>
