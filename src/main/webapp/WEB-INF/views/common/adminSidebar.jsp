
<div class="sidebar-nav">
    <div class="navbar navbar-default" role="navigation">
        <div class="navbar-header">
            <button id="adminSidebarButton" type="button" class="navbar-toggle" data-toggle="collapse"
                    data-target=".sidebar-navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <span class="visible-xs navbar-brand">Admin Panel</span>
        </div>
        <div class="navbar-collapse collapse sidebar-navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="text-center">
                    <h3>Admin Panel</h3>
                </li>
                <hr/>
                <li class="admin-sidebar-item ${ path eq "/events/add" ? "active" : "" }"><a id="addEvent" href="/events/add">Add Event</a></li>
                <li class="admin-sidebar-item ${ path eq "/events/manage" ? "active" : "" }"><a id="manageEvents" href="/events/manage">Manage Events</a></li>
                <hr/>
                <li class="admin-sidebar-item ${ path eq "/establishments/add" ? "active" : "" }"><a id="addEstablishment" href="/establishments/add">Add Establishment</a>
                <li class="admin-sidebar-item ${ path eq "/establishments/manage" ? "active" : "" }"><a id="manageEstablishments" href="/establishments/manage">Manage Establishments</a></li>
                <hr class="no-margin-bottom"/>
                <li>
                    <spring:url value="/logout" var="logout"/>
                    <form action="${ logout }" method="post"
                          class="navbar-form text-center">
                        <sec:csrfInput/>
                        <button type="submit" id="logout" class="btn btn-danger"><i class="fa fa-sign-out"></i> Log Out</button>
                    </form>
                </li>
            </ul>
        </div>
    </div>
</div>
