<%@taglib prefix="joda" uri="http://www.joda.org/joda/time/tags" %>

<c:import url="/WEB-INF/views/common/header.jsp">
    <c:param name="title" value="Mall World - Events"/>
</c:import>

<div class="container" id="wrapper">
    <c:import url="/WEB-INF/views/common/messages.jsp"/>
    <p><c:out value="${deletedEvent}"/></p>

    <c:forEach var="event" items="${myEvents}" varStatus="row">
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="heading${event.id}" style="background:${event.eventCategory.colour}">
                <h4 class="panel-title">

                    <a data-toggle="collapse" data-parent="#accordion"
                       href="#collapse${event.id}" aria-expanded="true"
                       aria-controls="collapse${event.id}">
                        <c:out value="${event.title}"/>
                        -- <joda:format value="${event.startTimeDate}" pattern="dd-MMM-yyyy HH:mm" style="F-"/>
                         - <joda:format value="${event.endTimeDate}" pattern="dd-MMM-yyyy HH:mm" style="F-"/>
                    </a>

                <form:form action="/myEvents/leave" method="post" cssClass="pull-right" >
                    <button type="submit" value="Submit" class="leaveEvent btn btn-default">Leave Event</button>
                    <input type="hidden" name ="eventId" value="${event.id}" />
                </form:form>
                <span class="clearfix"></span>
            </h4>
        </div>
        <div id="collapse${event.id}" class="panel-collapse collapse"
             role="tabpanel" aria-labelledby="heading${event.id}">
            <div class="panel-body">
                            <h4>Description</h4></br>
                            <c:out value="${event.description}"/></br>
                            <h4>Category</h4> </br>
                            <c:out value="${event.eventCategory.name}"/>
                            <h4>Special Instructions</h4> </br>
                            <c:out value="${event.specialInstructions}"/>
                            <h4>Establishment</h4> </br>
                            <c:out value="${establishment.name}"/>
                         </div>

            </div>
        </c:forEach>
    </div>
</div>

<c:import url="/WEB-INF/views/common/footer.jsp"/>
