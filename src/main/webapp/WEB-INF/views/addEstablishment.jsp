<c:import url="/WEB-INF/views/common/header.jsp">
    <c:param name="title" value="Mall World - Add Establishment"/>
</c:import>

<div class="container" id="wrapper">
    <c:import url="/WEB-INF/views/common/messages.jsp"/>

    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <c:import url="/WEB-INF/views/common/adminSidebar.jsp"/>
            </div>

            <div class="col-sm-9">
                <h3>Add Establishment</h3>
                <hr/>
                <form:form method="post" modelAttribute="establishmentForm" cssClass="form-horizontal">

                    <div class="form-group">
                        <form:label path="name" cssClass="col-sm-2">Name</form:label>
                        <div class="col-sm-4">
                            <form:input path="name" cssClass="form-control" required="required"/>
                            <form:errors id="longTitleError" path="name" element="div"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <form:label path="userForm.email" cssClass="col-sm-2">Email</form:label>
                        <div class="col-sm-4">
                            <form:input path="userForm.email" cssClass="form-control" required="required"
                                        autocomplete="off"/>
                            <form:errors id="sameEmailError" path="userForm.email" element="div"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <form:label path="userForm.password" cssClass="col-sm-2">Password</form:label>
                        <div class="col-sm-4">
                            <form:password path="userForm.password" cssClass="form-control" required="required"
                                           autocomplete="off"/>
                            <form:errors path="userForm.password" element="div"/>
                        </div>
                    </div>


                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-2">
                            <button type="submit" id="saveEstablishment" value="Submit" class="btn btn-default">Save
                                Establishment
                            </button>
                        </div>
                    </div>
                </form:form>
            </div>
        </div>
    </div>
</div>

<c:import url="/WEB-INF/views/common/footer.jsp"/>