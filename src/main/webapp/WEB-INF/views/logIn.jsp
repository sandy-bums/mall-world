<c:import url="/WEB-INF/views/common/header.jsp">
<c:param name="title" value="Mall World - Log In"/>
</c:import>

<div class="container" id="wrapper">
    <c:import url="/WEB-INF/views/common/messages.jsp"/>
    <div class="page-header">
        <h1>Welcome back!</h1>
    </div>

    <form:form action="/logIn" method="post" cssClass="form-horizontal">
        <sec:csrfInput/>
        <div class="form-group">
            <label for="email" class="col-sm-2 control-label">Email</label>
            <div class="col-sm-8">
                <input type="email" name="email" class="form-control" id="email" placeholder="Email">
            </div>
        </div>
        <div class="form-group">
            <label for="password" class="col-sm-2 control-label">Password</label>
            <div class="col-sm-8">
                <input type="password" name="password" class="form-control" id="password" placeholder="Password">
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-8">
                <button type="submit" id="submit" value="Submit" class="btn btn-primary">Log In</button>
            </div>
        </div>
    </form:form>

</div>

<c:import url="/WEB-INF/views/common/footer.jsp"/>