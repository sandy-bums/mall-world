<c:import url="/WEB-INF/views/common/header.jsp">
    <c:param name="title" value="Mall World"/>
</c:import>

<div class="container" id="wrapper">
    <c:import url="/WEB-INF/views/common/messages.jsp"/>
    <form:form method="get" modelAttribute="eventFiltersForm">
        <c:forEach var="category" items="${eventCategories}">
            <span class="sidebar-categories inline" style="background-color:${category.colour}"><form:checkbox id="${category.name}" path="selectedEventCategories" onChange="this.form.submit();" value="${category.name}"/>${category.name}</span>
        </c:forEach>
    </form:form>

    <script>

        $.getScript('http://arshaw.com/js/fullcalendar-1.6.4/fullcalendar/fullcalendar.min.js', function () {

            var eventsList = [
                <c:forEach var="event" items="${events}" varStatus="row">
                    {
                        title: "${event.title}",
                        start: new Date(Date.parse("${event.startTimeDate}".concat("+0100"))),
                        end: new Date(Date.parse("${event.endTimeDate}".concat("+0100"))),
                        color: "${event.eventCategory.colour}",
                        textColor: "black"
                    },
                </c:forEach>
            ]
            console.log(new Date(eventsList[0].start));
            var myEventsList = [
                <c:forEach var="event" items="${myEvents}" varStatus="row">
                    {
                        title: "${event.title}",
                        start: new Date("${event.startTimeDate}".concat("+0100")),
                        end: new Date("${event.endTimeDate}".concat("+0100")),
                        color: "pink",
                        borderColor: "black",
                        textColor: "black"
                    },
                </c:forEach>
            ]

            $('#calendar').fullCalendar({
                header: {
                    left: 'prev, next today',
                    center: 'title',
                    right: 'month, agendaWeek, agendaDay'
                },
                defaultView: 'agendaDay',
                editable: false,
                allDayDefault: false,
                eventSources: [eventsList, myEventsList]
            });
        })
    </script>
    <div class="container">
        <hr>
        <div id="calendar"></div>
    </div>

</div>

<c:import url="/WEB-INF/views/common/footer.jsp"/>
