package com.thoughtworks.sandybums.mallworld.service;

import com.thoughtworks.sandybums.mallworld.domain.EventCategory;
import com.thoughtworks.sandybums.mallworld.mappers.EventCategoryMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class EventCategoryService {


    private EventCategoryMapper eventCategoryMapper;

    @Autowired
    public EventCategoryService(EventCategoryMapper eventCategoryMapper) {
        this.eventCategoryMapper = eventCategoryMapper;
    }

    public List<EventCategory> getDefaultCategories() {
        return eventCategoryMapper.getDefaultCategories();
    }

    public EventCategory getCategoryByName(String category) {
        return eventCategoryMapper.getCategoryByName(category);
    }

    public List<String> getCategoryNames() {
        return getDefaultCategories()
                .stream()
                .map(EventCategory::getName)
                .collect(Collectors.toList());
    }
}
