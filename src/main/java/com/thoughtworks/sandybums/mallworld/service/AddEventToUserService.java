package com.thoughtworks.sandybums.mallworld.service;

import com.thoughtworks.sandybums.mallworld.domain.UserEvents;
import com.thoughtworks.sandybums.mallworld.mappers.AddEventToUserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class AddEventToUserService {

    private AddEventToUserMapper addEventToUserMapper;

    @Autowired
    public AddEventToUserService(AddEventToUserMapper addEventToUserMapper) {
        this.addEventToUserMapper = addEventToUserMapper;
    }

    public void addEventToUser(UserEvents myEvent) {
        addEventToUserMapper.saveEventToUser(myEvent);
    }

    public List<UserEvents> getUserEvents(Long mallWorldId) {
        return addEventToUserMapper.getUserEventsById(mallWorldId);
    }
}
