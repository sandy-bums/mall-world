package com.thoughtworks.sandybums.mallworld.service;

import com.thoughtworks.sandybums.mallworld.domain.Event;
import com.thoughtworks.sandybums.mallworld.domain.User;
import com.thoughtworks.sandybums.mallworld.mappers.EventMapper;
import com.thoughtworks.sandybums.mallworld.web.form.EventFiltersForm;
import org.joda.time.Interval;
import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class EventService {

    private EventMapper eventMapper;

    @Autowired
    public EventService(EventMapper eventMapper) {
        this.eventMapper = eventMapper;
    }

    public void saveEvent(Event event) {
        eventMapper.saveEvent(event);
    }

    public void removeEvent(Event event) {
        eventMapper.removeEvent(event);
    }

    public void removeEvent(Long id) {
        eventMapper.removeEvent(id);
    }

    public void removeUserFromEvent(Long id, User currentUser) {
        Event event = eventMapper.getEvent(id);
        event.removeAttendee(currentUser);
        eventMapper.update(event);
    }

    public List<Event> getEvents() {
        return eventMapper.getEvents();
    }

    public List<Event> getOngoingAndFutureEvents() {
        return eventMapper.getOngoingAndFutureEvents();
    }

    public List<Event> applySearchQuery(String searchQuery, List<Event> eventList) {
        List finalList = new ArrayList<>(eventList);
        if (searchQuery != null && searchQuery.length() < 250 && !searchQuery.isEmpty()) {
            finalList = eventList.stream()
                    .filter(event -> event.toString().toLowerCase().contains(searchQuery.toLowerCase().trim()))
                    .collect(Collectors.toList());

        }
        return finalList;
    }

    public List<Event> applyCategoryFilter(List<String> selectedEventCategories, List<Event> eventList) {
        List finalList = new ArrayList<>(eventList);
        if (selectedEventCategories != null) {
            finalList = eventList.stream()
                                 .filter(event -> selectedEventCategories.contains(event.getEventCategory().getName()))
                                 .collect(Collectors.toList());
        }
        return finalList;
    }


    public List<Event> applyDateFilter(LocalDateTime filterStartDate, LocalDateTime filterEndDate, List<Event> eventList) {
        List finalList = new ArrayList<>(eventList);
        if (filterStartDate != null && filterEndDate != null) {
            finalList = eventList.stream()
                    .filter(event -> haveCommonInterval(filterStartDate, filterEndDate, event.getStartTimeDate(), event.getEndTimeDate()))
                    .collect(Collectors.toList());
        }
        return finalList;
    }

    private boolean haveCommonInterval(LocalDateTime filterStartDate, LocalDateTime filterEndDate, LocalDateTime startTimeDate, LocalDateTime endTimeDate) {

        Interval filterInterval = new Interval(filterStartDate.toDateTime(), filterEndDate.toDateTime());
        Interval eventInterval = new Interval(startTimeDate.toDateTime(), endTimeDate.toDateTime());

        return eventInterval.overlaps(filterInterval);
    }

    public List<Event> applyFilters(EventFiltersForm eventFiltersForm, List<Event> eventList) {
        LocalDateTime filterStartDate = eventFiltersForm.getFilterStartDate();
        LocalDateTime filterEndDate = eventFiltersForm.getFilterEndDate();

        List<String> selectedEventCategories = eventFiltersForm.getSelectedEventCategories();

        String keyword = eventFiltersForm.getKeyword();

        List<Event> finalList = applyDateFilter(filterStartDate, filterEndDate, eventList);
        finalList = applyCategoryFilter(selectedEventCategories, finalList);
        finalList = applySearchQuery(keyword, finalList);

        return finalList;
    }

}

