package com.thoughtworks.sandybums.mallworld.service;


import com.thoughtworks.sandybums.mallworld.domain.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface UserContext {
    User getCurrentUser();
    void setCurrentUser(final User user);
    void logout(HttpServletRequest request, HttpServletResponse response);
}
