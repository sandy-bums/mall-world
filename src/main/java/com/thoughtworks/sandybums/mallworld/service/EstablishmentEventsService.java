package com.thoughtworks.sandybums.mallworld.service;


import com.thoughtworks.sandybums.mallworld.domain.EstablishmentEvents;
import com.thoughtworks.sandybums.mallworld.mappers.EstablishmentEventsMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class EstablishmentEventsService {

    private EstablishmentEventsMapper establishmentEventsMapper;

    @Autowired
    public EstablishmentEventsService(EstablishmentEventsMapper establishmentEventsMapper) {
        this.establishmentEventsMapper = establishmentEventsMapper;
    }

    public void saveEventToEstablishment(EstablishmentEvents establishmentEvents) {
        establishmentEventsMapper.saveEventToEstablishment(establishmentEvents);

    }
}
