package com.thoughtworks.sandybums.mallworld.service;

import com.thoughtworks.sandybums.mallworld.domain.Event;
import com.thoughtworks.sandybums.mallworld.domain.User;
import com.thoughtworks.sandybums.mallworld.mappers.AddEventToUserMapper;
import com.thoughtworks.sandybums.mallworld.mappers.EventMapper;
import com.thoughtworks.sandybums.mallworld.mappers.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.HashSet;
import java.util.Set;

@Service
@Transactional
public class UserService {
    private UserMapper userMapper;
    private AddEventToUserMapper addEventToUserMapper;
    private EventMapper eventMapper;

    @Autowired
    public UserService(UserMapper userMapper, EventMapper eventMapper, AddEventToUserMapper addEventToUserMapper) {
        this.userMapper = userMapper;
        this.eventMapper = eventMapper;
        this.addEventToUserMapper = addEventToUserMapper;
    }

    public void saveUser(User user) {
        userMapper.saveUser(user);
    }

    public void removeUser(User user) {
        userMapper.removeUser(user);
    }

    public User getUser(String email) {
        return userMapper.getUser(email);
    }

    public Set<Event> getUserEvents(User currentUser) {
        if (currentUser == null) {
            return new HashSet<>();
        }
        return userMapper.getUserWithEvents(currentUser.getEmail()).getAttendedEvents();
    }
}
