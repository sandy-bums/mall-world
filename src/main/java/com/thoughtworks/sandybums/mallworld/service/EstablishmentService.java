package com.thoughtworks.sandybums.mallworld.service;


import com.thoughtworks.sandybums.mallworld.domain.Establishment;
import com.thoughtworks.sandybums.mallworld.domain.Event;
import com.thoughtworks.sandybums.mallworld.domain.User;
import com.thoughtworks.sandybums.mallworld.mappers.EstablishmentMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Set;

@Service
@Transactional
public class EstablishmentService {

    private final EstablishmentMapper establishmentMapper;

    @Autowired
    public EstablishmentService(EstablishmentMapper establishmentMapper){
        this.establishmentMapper = establishmentMapper;
    }
    public void saveOwner(Establishment establishment){
        establishmentMapper.saveEstablishment(establishment);
    }
    public void removeOwner(Establishment establishment){
        establishmentMapper.removeEstablishment(establishment);
    }

    public Establishment getEstablishment(User user) {return establishmentMapper.getEstablishment(user);}

    public List<Establishment> getEstablishments(){
        return establishmentMapper.getEstablishments();
    }

    public Set<Event> getOwnedEventsForEstablishment(Establishment currentEstablishment) {
        return establishmentMapper.getOwnedEventsForEstablishment(currentEstablishment);
    }
}
