package com.thoughtworks.sandybums.mallworld.service;

import com.thoughtworks.sandybums.mallworld.domain.User;
import com.thoughtworks.sandybums.mallworld.mappers.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class UserInfoService implements UserDetailsService {

    private final UserMapper userMapper;

    @Autowired
    public UserInfoService(UserMapper userMapper){
        this.userMapper = userMapper;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User currentUser = userMapper.getUser(username);
        if(currentUser == null) {
            throw new UsernameNotFoundException("User " + username + " does not exist" );
        }
        return new org.springframework.security.core.userdetails.User(currentUser.getEmail(), currentUser.getPassword(), currentUser.getGrantedAuthority());
    }
}
