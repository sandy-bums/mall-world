package com.thoughtworks.sandybums.mallworld.service;


import com.thoughtworks.sandybums.mallworld.domain.User;
import com.thoughtworks.sandybums.mallworld.mappers.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;

@Service
@Transactional
public class UserContextService implements UserContext {

    private final UserMapper userMapper;
    private final UserDetailsService userDetailsService;

    @Autowired
    public UserContextService(UserMapper userMapper, UserDetailsService userDetailsService) {
        this.userMapper = userMapper;
        this.userDetailsService = userDetailsService;
    }

    @Override
    public User getCurrentUser() {

        final SecurityContext context = SecurityContextHolder.getContext();
        final Authentication authentication = context.getAuthentication();
        if (authentication == null) {
            return null;
        }
        return userMapper.getUser(authentication.getName());
    }

    @Override
    public void setCurrentUser(User user) {

        final UserDetails userDetails = userDetailsService.loadUserByUsername(user.getEmail());
        final Authentication authentication = new UsernamePasswordAuthenticationToken(userDetails, user.getPassword(), userDetails.getAuthorities());
        SecurityContextHolder.getContext().setAuthentication(authentication);

    }

    @Override
    public void logout(HttpServletRequest request, HttpServletResponse response) {

    }
}
