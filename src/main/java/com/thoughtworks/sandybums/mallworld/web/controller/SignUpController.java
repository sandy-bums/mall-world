package com.thoughtworks.sandybums.mallworld.web.controller;

import com.thoughtworks.sandybums.mallworld.domain.User;
import com.thoughtworks.sandybums.mallworld.service.UserContext;
import com.thoughtworks.sandybums.mallworld.service.UserService;
import com.thoughtworks.sandybums.mallworld.web.form.UserForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.validation.Valid;

@Controller
@RequestMapping("signUp")
public class SignUpController {

    private final UserService userService;
    private final UserContext userContext;
    private JavaMailSender mailSender;

    @Autowired
    public SignUpController(UserService userService, UserContext userContext, JavaMailSender mailSender) {
        this.userService = userService;
        this.userContext = userContext;
        this.mailSender = mailSender;
    }

    @RequestMapping(method = RequestMethod.GET)
    public String index(@ModelAttribute("user") UserForm user) {
        return "signUp";
    }

    @RequestMapping(method = RequestMethod.POST)
    public String signUp(@Valid @ModelAttribute("user") UserForm user, BindingResult result) {

        if (userService.getUser(user.getEmail()) != null) {
            result.rejectValue("email", "signUpPage.emailInUse");
        }

        if (result.hasErrors()) {
            return "signUp";
        }

        User currentUser = user.build();
        userService.saveUser(currentUser);
        userContext.setCurrentUser(currentUser);

        sendConfirmationEmail(currentUser);

        return "redirect:/";
    }

    private void sendConfirmationEmail(User currentUser) {
        MimeMessagePreparator preparator = mimeMessage -> {
            mimeMessage.setRecipient(Message.RecipientType.TO,
                    new InternetAddress(currentUser.getEmail()));
            mimeMessage.setSubject("Welcome to Mall World!");
            mimeMessage.setText("Hello! We hope you love Mall World :)\n");
        };

        try {
            mailSender.send(preparator);
        } catch (MailException ex) {
            System.err.println(ex.getMessage());
        }
    }

}
