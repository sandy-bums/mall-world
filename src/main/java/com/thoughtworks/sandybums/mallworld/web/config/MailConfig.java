package com.thoughtworks.sandybums.mallworld.web.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import java.util.Properties;

@Configuration
@ComponentScan(basePackages = {"com.thoughtworks.sandybums.mallworld.web.controller"})
public class MailConfig {

    @Value("smtp")
    private String protocol;

    @Value("smtp.gmail.com")
    private String host;

    private Integer port = 587;

    @Value("mallworld.venue@gmail.com")
    private String from;

    @Value("password@mallworld")
    private String password;

    @Bean
    public JavaMailSender javaMailSender() {
        JavaMailSenderImpl javaMailSender = new JavaMailSenderImpl();
        javaMailSender.setProtocol(protocol);
        javaMailSender.setHost(host);
        javaMailSender.setPort(port);
        javaMailSender.setUsername(from);
        javaMailSender.setPassword(password);

        Properties props = new Properties();
        props.put("mail.debug", "true");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");

        javaMailSender.setJavaMailProperties(props);
        return javaMailSender;
    }

}
