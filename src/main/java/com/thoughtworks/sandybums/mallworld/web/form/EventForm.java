package com.thoughtworks.sandybums.mallworld.web.form;

import com.thoughtworks.sandybums.mallworld.domain.Establishment;
import com.thoughtworks.sandybums.mallworld.domain.Event;
import com.thoughtworks.sandybums.mallworld.domain.EventCategory;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;
import org.joda.time.LocalDateTime;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.NotNull;

public class EventForm {

    //required parameters
    @NotEmpty
    @Length(max = 255)
    private String title;
    @Length(max = 255)
    @NotNull
    private String category;
    @NotNull
    private LocalDateTime startDate;
    @NotNull
    private LocalDateTime endDate;

    private Establishment establishment;

    private EventCategory eventCategory;


    //optional parameters
    private String description;
    @Length(max = 255)
    private String specialInstructions;

    public EventForm() {
    }

    @AssertTrue
    public boolean isStartDateBeforeEndDate() {
        if (startDate == null || endDate == null) {
            return false;
        }
        return endDate.compareTo(startDate) > 0;
    }

    @AssertTrue
    public boolean isStartEndYearValid(){
        if (startDate == null || endDate == null) {
            return false;
        }
        if(startDate.getYear()>9999 || endDate.getYear()>9999){
            return false;
        }
        return true;
    }

    public String getTitle() {
        return title;
    }

    public EventForm setTitle(String title) {
        this.title = title;
        return this;
    }

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public EventForm setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
        return this;
    }

    public LocalDateTime getEndDate() {
        return endDate;
    }

    public EventForm setEndDate(LocalDateTime endDate) {
        this.endDate = endDate;
        return this;
    }

    public Establishment getEstablishment() {
        return establishment;
    }

    public EventForm setEstablishment(Establishment establishment) {
        this.establishment = establishment;
        return this;
    }

    public String getCategory() {
        return category;
    }

    public EventForm setCategory(String category) {
        this.category = category;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public EventForm setDescription(String description) {
        this.description = description;
        return this;
    }

    public String getSpecialInstructions() {
        return specialInstructions;
    }

    public EventForm setSpecialInstructions(String specialInstructions) {
        this.specialInstructions = specialInstructions;
        return this;
    }

    public EventCategory getEventCategory() {
        return eventCategory;
    }

    public EventForm setEventCategory(EventCategory eventCategory) {
        this.eventCategory = eventCategory;
        return this;
    }

    public EventForm category(String val) {
        category = val;
        return this;
    }

    public EventForm description(String val) {
        description = val;
        return this;
    }

    public EventForm specialInstructions(String val) {
        specialInstructions = val;
        return this;
    }

    public Event build() {
        return new Event.EventBuilder(title, startDate, endDate, eventCategory, establishment).description(description).specialInstructions(specialInstructions).build();
    }
}
