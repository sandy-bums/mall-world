package com.thoughtworks.sandybums.mallworld.web.form;

import org.joda.time.LocalDateTime;

public class SelectedDatesForm {
    private LocalDateTime filterStartDate;
    private LocalDateTime filterEndDate;

    public LocalDateTime getFilterStartDate() {
        return filterStartDate;
    }

    public void setFilterStartDate(LocalDateTime filterStartDate) {
        this.filterStartDate = filterStartDate;
    }

    public LocalDateTime getFilterEndDate() {
        return filterEndDate;
    }

    public void setFilterEndDate(LocalDateTime filterEndDate) {
        this.filterEndDate = filterEndDate;
    }
}
