package com.thoughtworks.sandybums.mallworld.web.form;

import com.thoughtworks.sandybums.mallworld.domain.Role;
import com.thoughtworks.sandybums.mallworld.domain.User;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

public class UserForm {

    @NotEmpty(message = "Field cannot be empty.")
    @Email(message = "This is not a well-formed email address.")
    private String email;
    @Length(min = 8, max = 20, message = "Password length must be between 8 and 20 characters.")
    private String password;


    public String getEmail() {
        return email;
    }

    public UserForm setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public UserForm setPassword(String password) {
        this.password = password;
        return this;
    }

    public User build() {
        return new User(email, password, Role.VISITOR_ROLE);
    }

}
