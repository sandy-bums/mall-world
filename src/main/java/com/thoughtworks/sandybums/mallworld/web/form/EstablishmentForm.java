package com.thoughtworks.sandybums.mallworld.web.form;

import com.thoughtworks.sandybums.mallworld.domain.Establishment;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;

public class EstablishmentForm {
    @NotEmpty
    private String name;

    private UserForm userForm;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public UserForm getUserForm() {
        return userForm;
    }

    public void setUserForm(UserForm userForm) {
        this.userForm = userForm;
    }

    public Establishment build(){
        return new Establishment(name, userForm.build());
    }
}
