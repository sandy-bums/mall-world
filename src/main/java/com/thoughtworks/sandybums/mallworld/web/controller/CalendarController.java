package com.thoughtworks.sandybums.mallworld.web.controller;

import com.thoughtworks.sandybums.mallworld.domain.Event;
import com.thoughtworks.sandybums.mallworld.domain.EventCategory;
import com.thoughtworks.sandybums.mallworld.service.EventCategoryService;
import com.thoughtworks.sandybums.mallworld.service.EventService;
import com.thoughtworks.sandybums.mallworld.service.UserContext;
import com.thoughtworks.sandybums.mallworld.service.UserService;
import com.thoughtworks.sandybums.mallworld.web.form.EventFiltersForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Controller
@RequestMapping("/calendar")
public class CalendarController {

    @Autowired
    private EventService eventService;
    @Autowired
    private UserService userService;
    @Autowired
    private UserContext userContext;
    @Autowired
    private EventCategoryService eventCategoryService;

    @RequestMapping(method = RequestMethod.GET)
    public String index(@ModelAttribute("eventFiltersForm") EventFiltersForm eventFiltersForm, Model model) {
//        List<String> eventCategoryNames = eventCategoryService.getCategoryNames();
//        model.addAttribute("eventCategories", eventCategoryNames);

        List<EventCategory> eventCategories = eventCategoryService.getDefaultCategories();
        model.addAttribute("eventCategories", eventCategories);

        List<String> selectedEventCategories = eventFiltersForm.getSelectedEventCategories();

        List<Event> filteredList = eventService.applyCategoryFilter(selectedEventCategories, eventService.getEvents());

        model.addAttribute("events", filteredList);

        //model.addAttribute("events", eventService.getEvents());
        model.addAttribute("myEvents", userService.getUserEvents(userContext.getCurrentUser()));
        return "calendar";
    }

}
