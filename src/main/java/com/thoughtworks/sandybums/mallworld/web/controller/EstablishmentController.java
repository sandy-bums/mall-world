package com.thoughtworks.sandybums.mallworld.web.controller;

import com.thoughtworks.sandybums.mallworld.domain.Establishment;
import com.thoughtworks.sandybums.mallworld.service.EstablishmentService;
import com.thoughtworks.sandybums.mallworld.service.UserService;
import com.thoughtworks.sandybums.mallworld.web.form.EstablishmentForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Controller
@RequestMapping("/establishments")
public class EstablishmentController {

    private EstablishmentService establishmentService;
    private UserService userService;

    @Autowired
    public EstablishmentController(EstablishmentService establishmentService, UserService userService) {
        this.establishmentService = establishmentService;
        this.userService = userService;
    }

    @RequestMapping(method = RequestMethod.GET)
    public String index(Model model) {
        model.addAttribute("establishments", establishmentService.getEstablishments());
        return "establishments";
    }

    @RequestMapping(method = RequestMethod.GET, value = "add")
    public String establishment(@ModelAttribute("establishmentForm") EstablishmentForm establishmentForm) {
        return "addEstablishment";
    }

    @RequestMapping(method = RequestMethod.GET, value = "manage")
    public String manage(Model model) {
        List<Establishment> allEstablishments = establishmentService.getEstablishments();

        model.addAttribute("allEstablishments", allEstablishments);
        return "manageEstablishments";
    }

    @RequestMapping(method = RequestMethod.POST, value = "add")
    public String saveEstablishment(@ModelAttribute("establishmentForm") EstablishmentForm establishmentForm, BindingResult result) {
        if (userService.getUser(establishmentForm.getUserForm().getEmail()) != null) {
            result.rejectValue("userForm.email", "signUpPage.emailInUse");
        }

        if (result.hasErrors()) {
            return "addEstablishment";
        }

        establishmentService.saveOwner(establishmentForm.build());

        return "redirect:/establishments";
    }

}
