package com.thoughtworks.sandybums.mallworld.web.config;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@EnableTransactionManagement
@ComponentScan({"com.thoughtworks.sandybums.mallworld.mappers", "com.thoughtworks.sandybums.mallworld.service"})
@Import(DatabaseConfig.class)
public class ServiceConfig {

    @Bean
    @Autowired
    public LocalSessionFactoryBean getSessionFactory(
            final DataSource dataSource,
            @Value("${hibernate.dialect}") final String dialect,
            @Value("${hibernate.show_sql}") final String showSql) {
        final LocalSessionFactoryBean l = new LocalSessionFactoryBean();
        final Properties p = new Properties();
        p.setProperty("hibernate.dialect", dialect);
        p.setProperty("hibernate.show_sql", showSql);
        p.setProperty("hibernate.bytecode.use_reflection_optimizer", "true");
        p.setProperty("hibernate.jdbc.use_streams_for_binary", "false");
        l.setHibernateProperties(p);
        l.setDataSource(dataSource);
        l.setPackagesToScan("com.thoughtworks.sandybums.mallworld.domain");
        return l;
    }

    @Bean
    @Autowired
    public HibernateTransactionManager getTransactionManager(final SessionFactory sessionFactory) {
        return new HibernateTransactionManager(sessionFactory);
    }

}
