package com.thoughtworks.sandybums.mallworld.web.form;

import org.joda.time.LocalDateTime;

import java.util.List;

public class EventFiltersForm {
    private LocalDateTime filterStartDate;
    private LocalDateTime filterEndDate;
    private List<String> selectedEventCategories;
    private String keyword;

    public LocalDateTime getFilterStartDate() {
        return filterStartDate;
    }

    public void setFilterStartDate(LocalDateTime filterStartDate) {
        this.filterStartDate = filterStartDate;
    }

    public LocalDateTime getFilterEndDate() {
        return filterEndDate;
    }

    public void setFilterEndDate(LocalDateTime filterEndDate) {
        this.filterEndDate = filterEndDate;
    }

    public List<String> getSelectedEventCategories() {
        return selectedEventCategories;
    }

    public void setSelectedEventCategories(List<String> selectedEventCategories) {
        this.selectedEventCategories = selectedEventCategories;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }
}
