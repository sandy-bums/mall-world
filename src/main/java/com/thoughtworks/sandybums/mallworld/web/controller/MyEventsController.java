package com.thoughtworks.sandybums.mallworld.web.controller;

import com.thoughtworks.sandybums.mallworld.domain.Event;
import com.thoughtworks.sandybums.mallworld.domain.User;
import com.thoughtworks.sandybums.mallworld.domain.UserEvents;
import com.thoughtworks.sandybums.mallworld.service.AddEventToUserService;
import com.thoughtworks.sandybums.mallworld.service.EventService;
import com.thoughtworks.sandybums.mallworld.service.UserContext;
import com.thoughtworks.sandybums.mallworld.service.UserService;
import com.thoughtworks.sandybums.mallworld.web.form.EventFiltersForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;
import java.util.Set;

@Controller
@RequestMapping("myEvents")
public class MyEventsController {

    private EventService eventService;
    private UserService userService;
    private UserContext userContext;
    private AddEventToUserService addEventToUserService;

    @Autowired
    public MyEventsController(UserService userService, UserContext userContext, AddEventToUserService addEventToUserService, EventService eventService) {
        this.userService = userService;
        this.userContext = userContext;
        this.addEventToUserService = addEventToUserService;
        this.eventService = eventService;
    }

    @RequestMapping(method = RequestMethod.GET)
    public String index(Model model) {
        Set<Event> searchResults = userService.getUserEvents(userContext.getCurrentUser());

        model.addAttribute("myEvents", searchResults);

        return "myEvents";
    }


    @RequestMapping(value = "addToUser", method = RequestMethod.POST)
    public String addEventToUser(@RequestParam Long eventId) {

        User user = userContext.getCurrentUser();
        UserEvents userEvent = new UserEvents(user.getId(), eventId);
        addEventToUserService.addEventToUser(userEvent);

        return "redirect:/events";
    }

    @RequestMapping(value = "leave", method = RequestMethod.POST)
    public String removeEventFromUser(@RequestParam Long eventId) {

        eventService.removeUserFromEvent(eventId, userContext.getCurrentUser());

        return "redirect:/myEvents";
    }

    // Exception handling methods
    @ExceptionHandler(Exception.class)
    public ModelAndView databaseError(Exception exception) {
        System.err.println("******** " + exception.getMessage() + " ******** " + exception.getCause());
        ModelAndView mav = new ModelAndView();
        mav.addObject("sqlError", "This event is already being attended!");
        mav.setViewName("redirect:/events");
        return mav;
    }
}
