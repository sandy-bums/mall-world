package com.thoughtworks.sandybums.mallworld.web.controller;

import com.thoughtworks.sandybums.mallworld.domain.*;
import com.thoughtworks.sandybums.mallworld.service.*;
import com.thoughtworks.sandybums.mallworld.web.form.EventFiltersForm;
import com.thoughtworks.sandybums.mallworld.web.form.EventForm;
import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.awt.image.BufferedImage;
import java.beans.PropertyEditorSupport;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Set;

@Controller
@RequestMapping("/events")
public class EventController {

    private EventService eventService;
    private EventCategoryService eventCategoryService;
    private EstablishmentService establishmentService;
    private UserContext userContext;
    private EstablishmentEventsService establishmentEventsService;

    @Autowired
    public EventController(EventService eventService,
                           EventCategoryService eventCategoryService,
                           EstablishmentService establishmentService,
                           UserContext userContext,
                           EstablishmentEventsService establishmentEventsService) {
        this.eventService = eventService;
        this.eventCategoryService = eventCategoryService;
        this.establishmentService = establishmentService;
        this.userContext = userContext;
        this.establishmentEventsService = establishmentEventsService;

    }


    @RequestMapping(method = RequestMethod.GET)
    public String index(@ModelAttribute("eventFiltersForm") EventFiltersForm eventFiltersForm, HttpServletRequest request, Model model, BindingResult result) {

        String sqlErrorMessage = request.getParameter("sqlError");
        model.addAttribute("sqlError", sqlErrorMessage);

        List<EventCategory> eventCategories = eventCategoryService.getDefaultCategories();
        model.addAttribute("eventCategories", eventCategories);

        List<Event> eventList = eventService.getOngoingAndFutureEvents();

        if (eventFiltersForm.getFilterStartDate() != null && eventFiltersForm.getFilterEndDate() != null) {
            if (eventFiltersForm.getFilterEndDate().compareTo(eventFiltersForm.getFilterStartDate()) < 0) {
                result.rejectValue("filterEndDate", "eventsPage.filterEndDateEarlierThanStart");
                model.addAttribute("events", eventList);
                return "events";
            }
        }
        eventList = eventService.applyFilters(eventFiltersForm, eventList);
        model.addAttribute("events", eventList);
        return "events";
    }

    @RequestMapping(value = "add", method = RequestMethod.GET)
    public String event(@ModelAttribute("event") EventForm event, Model model) {

        List<EventCategory> eventCategories = eventCategoryService.getDefaultCategories();
        model.addAttribute("eventCategories", eventCategories);

        return "addEvent";
    }

    @RequestMapping(value = "manage", method = RequestMethod.GET)
    public String manage(Model model) {
        User currentUser = userContext.getCurrentUser();
        Establishment currentEstablishment = establishmentService.getEstablishment(currentUser);
        Set<Event> ownedEvents = establishmentService.getOwnedEventsForEstablishment(currentEstablishment);

        model.addAttribute("ownedEvents", ownedEvents);
        return "manageEvents";
    }

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(LocalDateTime.class, new PropertyEditorSupport() {

            @Override
            public String getAsText() {
                return getValue() == null ? "" : getValue().toString();
            }

            @Override
            public void setAsText(String text) throws IllegalArgumentException {
                try {
                    setValue(LocalDateTime.parse(text));
                } catch (IllegalArgumentException e) {

                }
            }
        });
    }

    @RequestMapping(value = "add", method = RequestMethod.POST)
    public String saveEvent(@Valid @ModelAttribute("event") EventForm eventForm, BindingResult result, Model model, @RequestParam("file") MultipartFile file) {
        eventForm.setEventCategory(eventCategoryService.getCategoryByName(eventForm.getCategory()));

        Establishment establishment = establishmentService.getEstablishment(userContext.getCurrentUser());

        eventForm.setEstablishment(establishment);

        if (result.hasErrors()) {
            return event(eventForm, model);
        }

        Event event = eventForm.build();

        eventService.saveEvent(event);

        establishmentEventsService.saveEventToEstablishment(new EstablishmentEvents(establishment.getId(), event.getId()));

//        try {
//            if (file != null && !file.isEmpty()) {
//                BufferedImage src = null;
//                src = ImageIO.read(new ByteArrayInputStream(file.getBytes()));
//                File destination = new File("src/main/webapp/resources/uploadedImages/eventImages/" + event.getId() + ".png"); // something like C:/Users/tom/Documents/nameBasedOnSomeId.png
//                ImageIO.write(src, "png", destination);
//            }
//        } catch (IOException e) {
//            result.reject(e.getMessage());
//            return event(eventForm, model);
//
//        }

        return "redirect:/admin";
    }

    @RequestMapping(value = "delete", method = RequestMethod.POST)
    public String deleteEvent(@RequestParam Long id, RedirectAttributes redirectAttributes) {
        eventService.removeEvent(id);
        redirectAttributes.addFlashAttribute("deletedEvent", "Event Deleted");
        return "redirect:/events/manage";
    }


    @RequestMapping(value = "search", method = RequestMethod.GET)
    public String processSearch(HttpServletRequest request, Model model) {
        String searchQuery = request.getParameter("search");
        List<Event> searchResults = eventService.applySearchQuery(searchQuery, eventService.getOngoingAndFutureEvents());
        model.addAttribute("events", searchResults);
        return "redirect:/events";
    }


}
