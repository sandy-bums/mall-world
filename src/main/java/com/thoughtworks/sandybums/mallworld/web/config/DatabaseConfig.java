package com.thoughtworks.sandybums.mallworld.web.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;

@Configuration
@PropertySource({
        "classpath:db/database.properties",
        "classpath:properties/config/hibernate.properties"})
public class DatabaseConfig {

    @Bean
    public static PropertySourcesPlaceholderConfigurer getPropertySourcesPlaceholderConfigurer() {
        PropertySourcesPlaceholderConfigurer p = new PropertySourcesPlaceholderConfigurer();
        return p;
    }

    @Bean(name = "dataSource")
    public DataSource getDataSource(
            @Value("${jdbc.driverClassName}") final String driverClassName,
            @Value("${jdbc.url}") final String url,
            @Value("${jdbc.username}") final String username,
            @Value("${jdbc.password}") final String password) {
        final DriverManagerDataSource d = new DriverManagerDataSource();
        d.setDriverClassName(driverClassName);
        d.setUrl(url);
        d.setUsername(username);
        d.setPassword(password);
        return d;
    }

}
