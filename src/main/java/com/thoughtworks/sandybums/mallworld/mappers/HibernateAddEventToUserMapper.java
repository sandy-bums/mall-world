package com.thoughtworks.sandybums.mallworld.mappers;

import com.thoughtworks.sandybums.mallworld.domain.User;
import com.thoughtworks.sandybums.mallworld.domain.UserEvents;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.postgresql.util.PSQLException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public class HibernateAddEventToUserMapper implements AddEventToUserMapper {

    private final SessionFactory sessionFactory;


    @Autowired
    public HibernateAddEventToUserMapper(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<UserEvents> getUserEventsById(Long mallWorldId) {
        return (List<UserEvents>) sessionFactory.getCurrentSession()
                .createCriteria(UserEvents.class)
                .add(Restrictions.eq("mallWorldUser", mallWorldId))
                .list();
    }

    @Override
    public void saveEventToUser(UserEvents userEvents) {
        Session currentSession = sessionFactory.getCurrentSession();
        currentSession.save(userEvents);
    }

    @Override
    public void removeEventFromUser(UserEvents userEvent) {
        Session currentSession = sessionFactory.getCurrentSession();
        currentSession.delete(userEvent);
    }

}
