package com.thoughtworks.sandybums.mallworld.mappers;

import com.thoughtworks.sandybums.mallworld.domain.EventCategory;

import java.util.List;

public interface EventCategoryMapper {
    List<EventCategory> getDefaultCategories();

    EventCategory getCategoryByName(String category);
}
