package com.thoughtworks.sandybums.mallworld.mappers;

import com.thoughtworks.sandybums.mallworld.domain.Event;

import java.util.List;

public interface EventMapper {

    Event getEvent(Long eventId);

    List<Event> getEvents();

    void saveEvent(Event event);

    void removeEvent(Event event);

    void removeEvent(Long id);

    List<Event> getOngoingAndFutureEvents();

    void update(Event eventYoga);
}
