package com.thoughtworks.sandybums.mallworld.mappers;

import com.thoughtworks.sandybums.mallworld.domain.User;

public interface UserMapper {

    User getUser(String email);

    void saveUser(User user);

    void removeUser(User user);

    User getUserWithEvents(String email);

    void updateUser(User user);
}
