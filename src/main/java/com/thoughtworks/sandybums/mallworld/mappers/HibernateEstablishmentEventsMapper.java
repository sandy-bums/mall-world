package com.thoughtworks.sandybums.mallworld.mappers;

import com.thoughtworks.sandybums.mallworld.domain.EstablishmentEvents;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
@Transactional
public class HibernateEstablishmentEventsMapper implements EstablishmentEventsMapper {

    private SessionFactory sessionFactory;

    @Autowired
    public HibernateEstablishmentEventsMapper(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void saveEventToEstablishment(EstablishmentEvents establishmentEvents) {
        Session currentSession = sessionFactory.getCurrentSession();
        currentSession.save(establishmentEvents);
    }

    @Override
    public void removeEstablishmentEvents(EstablishmentEvents establishmentEvents) {
        Session currentSession = sessionFactory.getCurrentSession();
        currentSession.delete(establishmentEvents);
    }
}
