package com.thoughtworks.sandybums.mallworld.mappers;

import com.thoughtworks.sandybums.mallworld.domain.Establishment;
import com.thoughtworks.sandybums.mallworld.domain.EstablishmentEvents;
import com.thoughtworks.sandybums.mallworld.domain.Event;
import com.thoughtworks.sandybums.mallworld.domain.User;
import org.hibernate.FetchMode;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.hibernate.type.StandardBasicTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Set;

@Repository
@Transactional
public class HibernateEstablishmentMapper implements EstablishmentMapper {

    private SessionFactory sessionFactory;

    @Autowired
    public HibernateEstablishmentMapper(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void saveEstablishment(Establishment establishment) {
        Session currentSession = sessionFactory.getCurrentSession();
        currentSession.save(establishment);

    }

    @Override
    public void removeEstablishment(Establishment establishment) {
        Session currentSession = sessionFactory.getCurrentSession();
        currentSession.delete(establishment);
    }

    @Override
    public Establishment getEstablishment(User user) {
        Session currentSession = sessionFactory.getCurrentSession();
        return (Establishment)currentSession
                .createCriteria(Establishment.class)
                .add(Restrictions.eq("mallWorldUser", user))
                .uniqueResult();
    }

    @Override
    public Set<Event> getOwnedEventsForEstablishment(Establishment establishment) {
        Session currentSession = sessionFactory.getCurrentSession();
        Establishment establishmentWithOwnedEvents = (Establishment) currentSession
                                          .createCriteria(Establishment.class)
                                          .add(Restrictions.eq("id", establishment.getId()))
                                          .setFetchMode("ownedEvents", FetchMode.JOIN)
                                          .uniqueResult();

        return establishmentWithOwnedEvents.getOwnedEvents();
    }

    @Override
    public List<Establishment> getEstablishments(){
        Session currentSession = sessionFactory.getCurrentSession();
        return currentSession.createCriteria(Establishment.class)
                .list();
    }




}
