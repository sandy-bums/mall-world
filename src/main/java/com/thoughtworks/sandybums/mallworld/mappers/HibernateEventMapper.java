package com.thoughtworks.sandybums.mallworld.mappers;

import com.thoughtworks.sandybums.mallworld.domain.Event;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public class HibernateEventMapper implements EventMapper {

    private SessionFactory sessionFactory;

    @Autowired
    public HibernateEventMapper(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Event getEvent(Long eventId) {
        return (Event) sessionFactory.getCurrentSession()
                .get(Event.class, eventId);
    }

    @Override
    public List<Event> getEvents() {
        Session currentSession = sessionFactory.getCurrentSession();
        return currentSession.createCriteria(Event.class)
                .list();
    }

    @Override
    public List<Event> getOngoingAndFutureEvents() {
        return (List<Event>) sessionFactory.getCurrentSession()
                .createCriteria(Event.class)
                .add(Restrictions.ge("endTimeDate", LocalDateTime.now()))
                .list();
    }

    @Override
    public void update(Event eventYoga) {
        sessionFactory.getCurrentSession().update(eventYoga);
    }

    @Override
    public void saveEvent(Event event) {
        Session currentSession = sessionFactory.getCurrentSession();
        currentSession.save(event);
    }

    @Override
    public void removeEvent(Event event) {
        Session currentSession = sessionFactory.getCurrentSession();
        currentSession.delete(event);
    }

    @Override
    public void removeEvent(Long id) {
        Event event = (Event) sessionFactory.getCurrentSession().load(Event.class, id);
        sessionFactory.getCurrentSession().delete(event);
    }


}
