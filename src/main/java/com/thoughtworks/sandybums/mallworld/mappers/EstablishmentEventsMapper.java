package com.thoughtworks.sandybums.mallworld.mappers;

import com.thoughtworks.sandybums.mallworld.domain.EstablishmentEvents;

public interface EstablishmentEventsMapper {

    void saveEventToEstablishment(EstablishmentEvents establishmentEvents);

    void removeEstablishmentEvents(EstablishmentEvents establishmentEvents);
}
