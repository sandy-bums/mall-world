package com.thoughtworks.sandybums.mallworld.mappers;


import com.thoughtworks.sandybums.mallworld.domain.EventCategory;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public class HibernateEventCategoryMapper implements EventCategoryMapper{

    private SessionFactory sessionFactory;

    @Autowired
    public HibernateEventCategoryMapper(SessionFactory sessionFactory){
        this.sessionFactory = sessionFactory;
    }


    @Override
    public List<EventCategory> getDefaultCategories() {
        return sessionFactory.getCurrentSession()
                             .createCriteria(EventCategory.class)
                             .list();
    }

    @Override
    public EventCategory getCategoryByName(String category) {
        return (EventCategory) sessionFactory.getCurrentSession()
                .createCriteria(EventCategory.class)
                .add(Restrictions.eq("name", category))
                .uniqueResult();
    }
}
