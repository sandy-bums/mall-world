package com.thoughtworks.sandybums.mallworld.mappers;

import com.thoughtworks.sandybums.mallworld.domain.Establishment;
import com.thoughtworks.sandybums.mallworld.domain.Event;
import com.thoughtworks.sandybums.mallworld.domain.User;

import java.util.List;
import java.util.Set;

public interface EstablishmentMapper {
    void saveEstablishment(Establishment establishment);
    void removeEstablishment(Establishment establishment);

    List<Establishment> getEstablishments();

    Establishment getEstablishment(User user);

    Set<Event> getOwnedEventsForEstablishment(Establishment establishment);
}
