package com.thoughtworks.sandybums.mallworld.mappers;

import com.thoughtworks.sandybums.mallworld.domain.UserEvents;
import org.postgresql.util.PSQLException;

import java.util.List;


public interface AddEventToUserMapper {

    List<UserEvents> getUserEventsById(Long mallWorldId);

    void saveEventToUser(UserEvents userEvents);

    void removeEventFromUser(UserEvents userEvent);

}
