package com.thoughtworks.sandybums.mallworld.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Role {

    public static final Role VISITOR_ROLE = new Role(1L, "ROLE_VISITOR");
    public static final Role ESTABLISHMENT_ROLE = new Role(2L, "ROLE_ESTABLISHMENT");
    public static final Role VENUE_ROLE = new Role(3L, "ROLE_VENUE");

    @Id
    @Column
    private final Long id;
    @Column
    private final String value;

    Role(){
        id = null;
        value = null;
    }

    private Role(Long id, String value) {
        this.id = id;
        this.value = value;
    }

    public String getValue() {
        return value;
    }

}
