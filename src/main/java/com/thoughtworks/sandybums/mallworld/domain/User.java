package com.thoughtworks.sandybums.mallworld.domain;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.persistence.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "mallWorldUser")
public class User {

    @Id
    @SequenceGenerator(name = "user_seq", sequenceName = "mallWorldUser_id_seq")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_seq")
    @Column
    private final Long id = null;
    @Column
    private final String email;
    @ManyToOne(optional = false)
    @JoinColumn(name = "role")
    private final Role role;
    @Column
    private String password;
    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "attendees")
    private Set<Event> attendedEvents = new HashSet<>();
    @Transient
    private List<? extends GrantedAuthority> grantedAuthority;

    User() {
        email = null;
        password = null;
        role = null;
    }

    public User(String email, String password, Role role) {
        if (email == null || email.isEmpty()) throw new IllegalArgumentException("Email must not be null or empty");
        if (password == null || password.isEmpty())
            throw new IllegalArgumentException("Password must not be null or empty");
        if (role == null) throw new IllegalArgumentException("Role must not be null");
        this.email = email;
        BCryptPasswordEncoder encryption = new BCryptPasswordEncoder(12);
        this.password = encryption.encode(password);
        this.role = role;
    }

    public Long getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    public List<? extends GrantedAuthority> getGrantedAuthority() {
        if (grantedAuthority == null) {
            grantedAuthority = AuthorityUtils.createAuthorityList(role.getValue());
        }
        return grantedAuthority;
    }

    public Set<Event> getAttendedEvents() {
        return attendedEvents;
    }

    public void setAttendedEvents(Set<Event> attendedEvents) {
        this.attendedEvents = attendedEvents;
    }

    public void attendEvent(Event event) {
        attendedEvents.add(event);
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        return email.equals(user.email);
    }

    @Override
    public int hashCode() {
        return email.hashCode();
    }
}
