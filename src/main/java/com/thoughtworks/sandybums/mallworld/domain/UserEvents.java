package com.thoughtworks.sandybums.mallworld.domain;

import javax.persistence.*;

@Entity
public class UserEvents {

    @Id
    @SequenceGenerator(name="user_events_seq", sequenceName="userevents_id_seq")
    @GeneratedValue(strategy = GenerationType.SEQUENCE ,generator="user_events_seq")
    @Column
    private final Long id = null;
    @Column
    private final Long mallWorldUser;
    @Column
    private final Long userEvent;

    UserEvents() {
        mallWorldUser = null;
        userEvent = null;
    }

    public UserEvents(Long mallWorldUser, Long userEvent) {
        this.mallWorldUser = mallWorldUser;
        this.userEvent = userEvent;
    }

    public Long getId() {
        return id;
    }

    public Long getMallWorldUser() {
        return mallWorldUser;
    }

    public Long getUserEvent() { return userEvent; }

    @Override
    public String toString() {
        return String.valueOf(mallWorldUser + "/" + userEvent);
    }

}