package com.thoughtworks.sandybums.mallworld.domain;

import javax.persistence.*;
import javax.validation.Valid;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
public class Establishment {

    @Id
    @SequenceGenerator(name="establishment_seq", sequenceName="owner_id_seq")
    @GeneratedValue(strategy = GenerationType.SEQUENCE ,generator="establishment_seq")
    @Column
    private final Long id = null;
    @Column
    private String name;
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "mallWorldUser")
    @Valid
    private User mallWorldUser;

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "owners")
    private Set<Event> ownedEvents = new HashSet<>();

    Establishment() {
        name = null;
        mallWorldUser = null;
    }

    public Establishment(String name, User user) {
        this.mallWorldUser = user;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }


    public User getMallWorldUser() {
        return mallWorldUser;
    }


    public Set<Event> getOwnedEvents() {
        return ownedEvents;
    }


}
