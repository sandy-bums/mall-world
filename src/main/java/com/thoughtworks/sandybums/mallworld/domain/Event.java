package com.thoughtworks.sandybums.mallworld.domain;

import org.hibernate.annotations.Type;
import org.joda.time.LocalDateTime;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Event {

    @Id
    @SequenceGenerator(name = "event_seq", sequenceName = "event_id_seq")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "event_seq")
    @Column
    private final Long id = null;
    @Column
    private final String title;
    @Column(name = "start_time_date")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
    private final LocalDateTime startTimeDate;
    @Column(name = "end_time_date")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
    private final LocalDateTime endTimeDate;
    @Column
    private String description;
    @Column
    private final String owner;
    @Column(name = "special_instructions")
    private String specialInstructions;
    @ManyToOne(optional = false)
    @JoinColumn(name = "eventCategory")
    private EventCategory eventCategory;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "userevents", joinColumns = {
            @JoinColumn(name = "userEvent")}, inverseJoinColumns = {
            @JoinColumn(name = "mallworlduser")
    })
    private Set<User> attendees = new HashSet<>();

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "establishmentEvents", joinColumns = {
            @JoinColumn(name = "event")}, inverseJoinColumns = {
            @JoinColumn(name = "establishment")
    })
    private Set<Establishment> owners = new HashSet<>();


    Event() {
        title = null;
        startTimeDate = null;
        endTimeDate = null;
        owner = null;
        eventCategory = null;
    }

    private Event(EventBuilder eventBuilder) {
        title = eventBuilder.title;
        startTimeDate = eventBuilder.startDate;
        endTimeDate = eventBuilder.endDate;
        description = eventBuilder.description;
        owner = eventBuilder.establishment.getName();
        specialInstructions = eventBuilder.specialInstructions;
        eventCategory = eventBuilder.eventCategory;
    }

    public void addAttendee(User user) {
        attendees.add(user);
    }

    public void removeAttendee(User currentUser) {
        attendees.remove(currentUser);
    }


    public static class EventBuilder {
        //required parameters
        private final String title;
        private final LocalDateTime startDate;
        private final LocalDateTime endDate;

        private final Establishment establishment;
        private final EventCategory eventCategory;

        //optional parameters
        private String description;
        private String specialInstructions;

        public EventBuilder(String title, LocalDateTime startTimeDate, LocalDateTime endTimeDate, EventCategory eventCategory, Establishment establishment) {
            this.title = title;
            this.startDate = startTimeDate;
            this.endDate = endTimeDate;
            this.establishment = establishment;
            this.eventCategory = eventCategory;
        }

        public String getTitle() {
            return title;
        }

        public Establishment getEstablishment() {
            return establishment;
        }

        public String getDescription() {
            return description;
        }

        public EventCategory getEventCategory() {
            return eventCategory;
        }

        public EventBuilder description(String val) {
            description = val;
            return this;
        }

        public EventBuilder specialInstructions(String val) {
            specialInstructions = val;
            return this;
        }

        public Event build() {
            return new Event(this);
        }

    }


    public Long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }


    public LocalDateTime getStartTimeDate() {
        return startTimeDate;
    }

    public LocalDateTime getEndTimeDate() {
        return endTimeDate;
    }

    public String getDescription() {
        return description;
    }

    public String getOwner() {
        return owner;
    }

    public String getSpecialInstructions() {
        return specialInstructions;
    }

    public EventCategory getEventCategory() {
        return eventCategory;
    }

    public Set<User> getAttendees() {
        return attendees;
    }

    @Override
    public String toString() {
        return title + eventCategory.getName() + description + owner;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Event event = (Event) o;

        if (id != null ? !id.equals(event.id) : event.id != null) return false;
        if (!title.equals(event.title)) return false;
        if (eventCategory.getName() != null ? !eventCategory.getName().equals(event.eventCategory.getName()) : event.eventCategory.getName() != null) return false;
        if (!startTimeDate.equals(event.startTimeDate)) return false;
        if (!endTimeDate.equals(event.endTimeDate)) return false;
        if (description != null ? !description.equals(event.description) : event.description != null) return false;
        if (!owner.equals(event.owner)) return false;
        return !(specialInstructions != null ? !specialInstructions.equals(event.specialInstructions) : event.specialInstructions != null);

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + title.hashCode();
        result = 31 * result + (eventCategory.getName() != null ? eventCategory.getName().hashCode() : 0);
        result = 31 * result + startTimeDate.hashCode();
        result = 31 * result + endTimeDate.hashCode();
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + owner.hashCode();
        result = 31 * result + (specialInstructions != null ? specialInstructions.hashCode() : 0);
        return result;
    }
}
