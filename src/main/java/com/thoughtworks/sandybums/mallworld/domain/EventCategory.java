package com.thoughtworks.sandybums.mallworld.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class EventCategory {


    @Column
    @Id
    private final short id;
    @Column
    private final String name;
    @Column
    private final String colour;


    EventCategory() {
        id = 0;
        name = null;
        colour = null;
    }

    public EventCategory(short id, String name, String colour) {
        this.id = id;
        this.name = name;
        this.colour = colour;
    }

    public short getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getColour() {
        return colour;
    }
}
