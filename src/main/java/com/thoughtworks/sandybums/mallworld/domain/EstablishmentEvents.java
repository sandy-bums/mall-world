package com.thoughtworks.sandybums.mallworld.domain;

import javax.persistence.*;

@Entity
public class EstablishmentEvents {
    @Id
    @SequenceGenerator(name="establishment_events_seq", sequenceName="establishmentevents_id_seq")
    @GeneratedValue(strategy = GenerationType.SEQUENCE ,generator="establishment_events_seq")
    @Column
    private final Long id = null;
    @Column
    private final Long establishment;
    @Column
    private final Long event;

    EstablishmentEvents() {
        establishment = null;
        event = null;
    }

    public EstablishmentEvents(Long establishment, Long event) {
        this.establishment = establishment;
        this.event = event;
    }

    public Long getId() {
        return id;
    }

    public Long getEstablishment() {
        return establishment;
    }

    public Long getEvent() { return event; }

    @Override
    public String toString() {
        return String.valueOf(establishment + "/" + event);
    }
}
