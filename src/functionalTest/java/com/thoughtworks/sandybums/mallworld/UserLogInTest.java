package com.thoughtworks.sandybums.mallworld;

import org.junit.Test;

public class UserLogInTest extends SeleniumTest{
    @Test
    public void testUserLoginJourney(){
        // Given there is a registered user
        signUpPage
                .visit()
                .fillForm("user@email.com", "password")
                .submit();
        header
                .clickLogout();

        // Clicks log in link in header
        homepage
                .visit();
        header
                .clickLogIn();
        logInPage
                .isCurrentPage();

        // Shows invalid credentials message

        // Invalid email
        logInPage
                 .fillForm("bad_user@email.com", "password")
                 .submit()
                 .isCurrentPage()
                 .showsInvalidCredentialsMessage();

        // Invalid password
        logInPage
                .fillForm("user@email.com", "bad_password")
                .submit()
                .isCurrentPage()
                .showsInvalidCredentialsMessage();

        // Successful login
        logInPage
                .fillForm("user@email.com", "password")
                .submit();
        homepage
                .isCurrentPage();
        header
                .displaysCorrectMenuItemsForLoggedInVisitorUser()
                .clickLogout();

        // Successful login for venue user
        header
                .clickLogIn();
        logInPage
                .fillForm("venue_user@venue.com", "password")
                .submit();
        header
                .displaysCorrectMenuItemsForLoggedInVenueUser();


    }
}
