package com.thoughtworks.sandybums.mallworld;

import org.junit.Test;

public class UserSignUpTest extends SeleniumTest {

    @Test
    public void testUserSignUpJourney() throws Exception {

        // Clicks sign up link in header
        homepage
                .visit();
        header
                .clickSignUp();
        signUpPage
                .isCurrentPage();

        // Shows invalid email format error
        signUpPage
                .fillForm("invalidEmail", "password")
                .submit()
                .isCurrentPage()
                .showsEmailInvalidMessage();

        // Shows required fields error
        signUpPage
                .fillForm("", "")
                .submit()
                .isCurrentPage()
                .showsRequiredFieldsMessage();

        // Successful sign up
        signUpPage
                .visit()
                .fillForm("test@email.com", "password")
                .submit();
        homepage
                .isCurrentPage();
        header
                .clickLogout();

        // Shows email already exists message
        header
                .clickSignUp();
        signUpPage
                .fillForm("test@email.com", "password")
                .submit()
                .isCurrentPage()
                .showsEmailInUseMessage();

    }

}
