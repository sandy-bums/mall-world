package com.thoughtworks.sandybums.mallworld.helpers;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;

public class EventsManagePage extends BasePage {

    private static final String URL = BASE_URL + "/events/manage";
    private final By delete = By.id("deleteEvent");
    private final By panelHeading = By.className("panel-heading");

    public EventsManagePage(WebDriver driver) {
        super(driver);
    }

    public EventsManagePage visit() {
        driver.get(URL);
        return this;
    }

    public EventsManagePage deleteLastAddedEvent() {
        List<WebElement> deleteEvents = driver.findElements(delete);
        deleteEvents.get(deleteEvents.size() - 1).click();
        return this;
    }

    public EventsManagePage lastEventIsNot(String title) {
        List<WebElement> eventList = getEventsOnPage();
        //TODO:check if the titles are unique
        if (eventList.size() != 0) {
            assertFalse(eventList.get(eventList.size() - 1).getText().contains(title));
        }
        return this;
    }

    private List<WebElement> getEventsOnPage() {
        return driver.findElements(panelHeading);
    }

    public EventsManagePage lastEventIs(String title) {
        List<WebElement> eventList = getEventsOnPage();
        assertThat(eventList.get(eventList.size() - 1).getText(), containsString(title));
        return this;
    }
}
