package com.thoughtworks.sandybums.mallworld.helpers;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

public class AddEstablishmentsPage extends BasePage{

    public static final String URL = BASE_URL + "/establishments/add";

    private final By name;
    private final By userEmail;
    private final By userPassword;
    private final By userExistsErrorMessage;
    private final By saveEstablishment;


    public AddEstablishmentsPage(WebDriver driver) {
        super(driver);
        name = By.name("name");
        userEmail = By.name("userForm.email");
        userPassword = By.name("userForm.password");
        userExistsErrorMessage = By.id("sameEmailError");
        saveEstablishment = By.id("saveEstablishment");
    }

    public AddEstablishmentsPage defaultFill(){
        driver.findElement(name).sendKeys("Test establishment");
        driver.findElement(userEmail).sendKeys("establishment@email.com");
        driver.findElement(userPassword).sendKeys("password");
        return this;
    }

    public AddEstablishmentsPage defaultFill(String establishmentName, String email, String password){
        driver.findElement(name).sendKeys(establishmentName);
        driver.findElement(userEmail).sendKeys(email);
        driver.findElement(userPassword).sendKeys(password);
        return this;
    }

    public AddEstablishmentsPage visit(){
        driver.get(URL);
        return this;
    }

    public AddEstablishmentsPage submit(){
        driver.findElement(saveEstablishment).click();
        return this;
    }

    public  AddEstablishmentsPage showUserEmailMessage(String errorMessage){
        assertThat(driver.findElement(userExistsErrorMessage).getText(), containsString(errorMessage));
        return this;
    }
}
