package com.thoughtworks.sandybums.mallworld.helpers;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

public class LogInPage extends BasePage {
    public static final String URL = BASE_URL + "/logIn";

    private final By email;
    private final By password;
    private final By submit;

    public LogInPage(WebDriver driver) {
        super(driver);
        email = By.name("email");
        password = By.name("password");
        submit = By.id("submit");
    }

    public LogInPage fillForm(String email, String password) {
        fillField(email, this.email);
        fillField(password, this.password);
        return this;
    }

    public LogInPage submit() {
        driver.findElement(submit).click();
        return this;
    }

    public LogInPage visit() {
        driver.get(URL);
        return this;
    }

    public LogInPage isCurrentPage() {
        isCurrentPage(URL);
        return this;
    }

    public LogInPage showsInvalidCredentialsMessage() {
        assertThat(driver.findElement(By.id("authenticationError")).getText(), containsString("Failed to login."));
        return this;
    }
}
