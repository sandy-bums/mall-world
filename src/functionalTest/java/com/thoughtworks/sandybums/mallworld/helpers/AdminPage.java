package com.thoughtworks.sandybums.mallworld.helpers;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class AdminPage extends BasePage {

    public static final String URL = BASE_URL + "/admin";
    private final By addEvent = By.id("addEvent");
    private final By manageEvent = By.id("manageEvents");
    private final By addEstablishment = By.id("addEstablishment");
    private final By manageEstablishment = By.id("manageEstablishments");


    public AdminPage(WebDriver driver) {
        super(driver);
    }

    public AdminPage visit() {
        driver.get(URL);
        return this;
    }

    public AdminPage isCurrentPage() {
        isCurrentPage(URL);
        return this;
    }

    public AdminPage displaysManageEstablishmentsButton() {
        findElement(manageEstablishment);
        return this;
    }

    public AdminPage displaysAddEstablishmentButton() {
        findElement(addEstablishment);
        return this;
    }

    public AdminPage displaysManageEventsButton() {
        findElement(manageEvent);
        return this;
    }

    public AdminPage displaysAddEventButton() {
        findElement(addEvent);
        return this;
    }

    public AdminPage clickManageEstabLink() {
        findAdminSideBarElement(manageEstablishment).click();
        return this;
    }

    public AdminPage clickAddEventButton() {
        findAdminSideBarElement(addEvent);
        findElement(addEvent).click();
        return this;
    }
}
