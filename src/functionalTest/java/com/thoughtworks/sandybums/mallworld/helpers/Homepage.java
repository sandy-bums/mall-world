package com.thoughtworks.sandybums.mallworld.helpers;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class Homepage extends BasePage {

    public static final String URL = BASE_URL + "/";

    public Homepage(WebDriver driver) {
        super(driver);
    }

    public Homepage visit() {
        driver.get(URL);
        return this;
    }

    public Homepage isCurrentPage() {
        isCurrentPage(URL);
        return this;
    }

    public void clickButton(String id) {
        driver.findElement(By.id(id)).click();
    }
}
