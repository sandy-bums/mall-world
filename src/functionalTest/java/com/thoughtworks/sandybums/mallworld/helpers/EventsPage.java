package com.thoughtworks.sandybums.mallworld.helpers;

import org.joda.time.LocalDateTime;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;

public class EventsPage extends BasePage {

    public static final String URL = BASE_URL + "/events";

    private final By panelHeading = By.className("panel-heading");
    private final By panelBody = By.className("panel-body");
    private final By sidebarButton = By.id("sidebarButton");
    private final By attendEventButton = By.id("addEventToUser");
    private final By filterStartDate = By.name("filterStartDate");
    private final By filterEndDate = By.name("filterEndDate");
    private final By sideBarFilterButton = By.id("filterByDate");

    public EventsPage(WebDriver driver) {
        super(driver);
    }


    public EventsPage filterByDate(LocalDateTime filterStartDate, LocalDateTime filterEndDate) {
        WebElement filterButton = findSideBarElement(sideBarFilterButton);
        findElement(this.filterStartDate).sendKeys(filterStartDate.toString());
        findElement(this.filterEndDate).sendKeys(filterEndDate.toString());
        filterButton.click();
        return this;
    }

    private List<WebElement> getEventsOnPage() {
        return findElements(panelHeading);
    }

    public EventsPage lastEventIs(String title) {
        List<WebElement> eventList = getEventsOnPage();
        assertThat(eventList.get(eventList.size() - 1).getText(), containsString(title));
        return this;
    }

    public EventsPage hasDatePresent(String startTime, String endTime) {
        List<WebElement> eventList = getEventsOnPage();
        assertThat(eventList.get(eventList.size() - 1).getText(), containsString(startTime));
        assertThat(eventList.get(eventList.size() - 1).getText(), containsString(endTime));
        return this;
    }

    public EventsPage lastEventIsNot(String title) {
        List<WebElement> eventList = getEventsOnPage();
        //TODO:check if the titles are unique
        if (eventList.size() != 0) {
            assertFalse(eventList.get(eventList.size() - 1).getText().contains(title));
        }
        return this;
    }

    public EventsPage visit() {
        driver.get(URL);
        return this;
    }

    public EventsPage isCurrentPage() {
        isCurrentPage(URL);
        return this;
    }

    public void attendFirstEvent() {
        List<WebElement> eventList = getEventsOnPage();
        assertFalse(eventList.size() == 0);
        eventList.get(0).findElement(attendEventButton).submit();
    }

    public EventsPage hasCategoryPresent() {
        assertThat(findElement(panelBody).getText(), containsString("Category"));
        return this;
    }

    public EventsPage renderEventsDetails(String title) {
        findElement(By.partialLinkText(title)).click();
        return this;
    }

    public EventsPage hasColour(String colour) {
        List<WebElement> eventList = getEventsOnPage();
        assertThat(eventList.get(eventList.size() - 1).getCssValue("background"), is(colour));
        return this;
    }

    public EventsPage selectCategoryFilters(String category) {
        findSideBarElement(By.id(category)).click();
        findSideBarElement(By.id("filterByDate")).click();

        return this;
    }

    public EventsPage hasOwnerPresent(String owner) {
        assertThat(findElement(panelHeading).getText(), containsString(owner));
        return this;
    }

    public EventsPage thereIsNo500Error() {
        findElement(sidebarButton);
        return this;
    }

}
