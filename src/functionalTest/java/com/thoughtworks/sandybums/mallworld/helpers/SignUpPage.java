package com.thoughtworks.sandybums.mallworld.helpers;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

public class SignUpPage extends BasePage {

    public static final String URL = BASE_URL + "/signUp";

    private final By email;
    private final By password;
    private final By submit;

    public SignUpPage(WebDriver driver) {
        super(driver);
        email = By.name("email");
        password = By.name("password");
        submit = By.id("submit");
    }

    public SignUpPage fillForm(String email, String password) {
        fillField(email, this.email);
        fillField(password, this.password);
        return this;
    }

    public SignUpPage submit() {
        driver.findElement(submit).click();
        return this;
    }

    public SignUpPage visit() {
        driver.get(URL);
        return this;
    }

    public SignUpPage isCurrentPage() {
        isCurrentPage(URL);
        return this;
    }

    public SignUpPage showsEmailInvalidMessage() {
        assertThat(driver.findElement(By.id("email.errors")).getText(), containsString("This is not a well-formed email address."));
        return this;
    }

    public SignUpPage showsRequiredFieldsMessage() {
        assertThat(driver.findElement(By.id("email.errors")).getText(), containsString("Field cannot be empty."));
        assertThat(driver.findElement(By.id("password.errors")).getText(), containsString("Password length must be between 8 and 20 characters."));
        return this;
    }

    public SignUpPage showsEmailInUseMessage() {
        assertThat(driver.findElement(By.id("email.errors")).getText(), containsString("Email already in use."));
        return this;
    }
}
