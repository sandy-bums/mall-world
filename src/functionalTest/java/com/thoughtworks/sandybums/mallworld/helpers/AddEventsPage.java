package com.thoughtworks.sandybums.mallworld.helpers;

import org.joda.time.LocalDateTime;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.MatcherAssert.assertThat;


public class AddEventsPage extends BasePage {

    public static final String URL = BASE_URL + "/events/add";

    private final By category = By.name("category");
    private final By title = By.name("title");
    private final By specialInstructions = By.name("specialInstructions");
    private final By startDate = By.name("startDate");
    private final By endDate = By.name("endDate");
    private final By saveEvent = By.id("saveEvent");
    private final By endError = By.id("startDateBeforeEndDate.errors");
    private final By titleError = By.id("title.errors");
    private final By uploadEventImageButton = By.id("uploadEventImage");

    public AddEventsPage(WebDriver driver) {
        super(driver);
    }

    public AddEventsPage isCurrentPage(){
        isCurrentPage(URL);
        return this;
    }

    public AddEventsPage defaultFill(String titleTest) {
        new Select(findElement(category)).selectByVisibleText("Fun & Games");
        findElement(title).sendKeys(titleTest);
        findElement(specialInstructions).sendKeys("some instructions");
        findElement(startDate).sendKeys(LocalDateTime.now().plusDays(1).toString());
        findElement(endDate).sendKeys(LocalDateTime.now().plusDays(2).toString());
        return this;
    }

    public AddEventsPage defaultFill(String titleTest, LocalDateTime startDateTime, LocalDateTime endDateTime, String eventCategory) {
        new Select(findElement(category)).selectByVisibleText(eventCategory);
        findElement(title).sendKeys(titleTest);
        findElement(specialInstructions).sendKeys("some instructions");
        findElement(startDate).sendKeys(startDateTime.toString());
        findElement(endDate).sendKeys(endDateTime.toString());
        return this;
    }

    public AddEventsPage saveEvent() {
       findElement(saveEvent).click();
        return this;
    }

    public AddEventsPage visit() {
        driver.get(URL);
        return this;
    }

    public AddEventsPage showsDateErrorMessage(String errorMessage) {
        assertThat(findElement(endError).getText(), containsString(errorMessage));
        return this;
    }


    public AddEventsPage showsTitleErrorMessage(String errorMessage) {
        assertThat(findElement(titleError).getText(), containsString(errorMessage));
        return this;
    }

    public AddEventsPage showsUploadImageButton(){
        elementExists(uploadEventImageButton);
        return this;
    }
}
