package com.thoughtworks.sandybums.mallworld.helpers;

import org.openqa.selenium.WebDriver;

public class EstablishmentsManagePage extends BasePage {

    private static final String URL = BASE_URL + "/establishments/manage";

    public EstablishmentsManagePage(WebDriver driver) {
        super(driver);
    }

    public EstablishmentsManagePage isCurrentPage() {
        isCurrentPage(URL);
        return this;
    }
}
