package com.thoughtworks.sandybums.mallworld.helpers;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.core.Is.is;

public class Header extends BasePage {

    private final By username = By.id("userName");
    private final By logoutButton = By.id("logout");
    private final By searchSubmit = By.id("searchSubmit");
    private final By signUpButton = By.id("signUp");
    private final By searchInput = By.id("searchText");
    private final By logInButton = By.id("logIn");
    private final By addEstablishment = By.id("addEstablishment");
    private final By adminButton = By.id("adminLink");
    private final By userDropdown = By.id("userDropdown");

    public Header(WebDriver driver) {
        super(driver);
    }


    public Header clickSignUp() {
        findHeaderNavBarElement(signUpButton).click();
        return this;
    }

    public Header clickSearchSubmit() {
        findHeaderNavBarElement(searchSubmit).click();
        return this;
    }

    public Header entersTextIntoSearchBox(String searchText) {
        findHeaderNavBarElement(searchInput).sendKeys(searchText);
        return this;
    }

    public Header isLoggedInWith(String email) {
        assertThat(findHeaderNavBarElement(username).getText(), is(email));
        return this;
    }

    public Header clickLogout() {
        findHeaderNavBarElement(userDropdown).click();
        findHeaderNavBarElement(logoutButton).submit();
        return this;
    }

    public Header clickLogIn() {
        findHeaderNavBarElement(logInButton).click();
        return this;
    }

    private void menuItemIsPresent(By by) {
        assertThat(driver.findElements(by), hasSize(1));
    }

    private void menuItemIsAbsent(By by) {
        assertThat(driver.findElements(by), hasSize(0));
    }

    private Header displaysCorrectMenuItemsForLoggedInUser() {
        menuItemIsPresent(userDropdown);
        menuItemIsAbsent(logInButton);
        menuItemIsAbsent(signUpButton);
        return this;
    }

    public Header displaysCorrectMenuItemsForLoggedInVisitorUser() {
        displaysCorrectMenuItemsForLoggedInUser();
        menuItemIsAbsent(adminButton);
        return this;
    }

    public Header displaysCorrectMenuItemsForLoggedInVenueUser() {
        displaysCorrectMenuItemsForLoggedInUser();
        return this;
    }

    public Header displaysAdminButton() {
        menuItemIsPresent(adminButton);
        return this;
    }

    public Header clickAdminButton() {
        findHeaderNavBarElement(adminButton).click();
        return this;
    }
}
