package com.thoughtworks.sandybums.mallworld.helpers;


import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertFalse;

public class MyEventsPage extends BasePage {

    private static final String URL = BASE_URL + "/myEvents";

    private final By panelHeading = By.className("panel-heading");

    public MyEventsPage(WebDriver driver) {
        super(driver);
    }

    private List<WebElement> getEventsOnPage() {
        return driver.findElements(panelHeading);
    }

    public MyEventsPage lastEventIs(String title) {
        List<WebElement> eventList = getEventsOnPage();
        assertThat(eventList.get(eventList.size() - 1).getText(), containsString(title));
        return this;
    }

    public MyEventsPage visit() {
        driver.get(URL);
        return this;
    }

    public MyEventsPage isCurrentPage() {
        isCurrentPage(URL);
        return this;
    }

    public MyEventsPage clickLeaveEventButton(String title) {
        List<WebElement> eventList = getEventsOnPage();
        assertThat(eventList, hasSize(1));
        eventList.get(0).findElement(By.className("leaveEvent")).click();
        return this;
    }

    public MyEventsPage lastEventIsNot(String title) {
        List<WebElement> eventList = getEventsOnPage();
        //TODO:check if the titles are unique
        if (eventList.size() != 0) {
            assertFalse(eventList.get(eventList.size() - 1).getText().contains(title));
        }
        return this;
    }

    public MyEventsPage hasColour(String colour) {
        List<WebElement> eventList = getEventsOnPage();
        Assert.assertThat(eventList.get(eventList.size() - 1).getCssValue("background"), is(colour));
        return this;
    }
}
