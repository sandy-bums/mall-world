package com.thoughtworks.sandybums.mallworld.helpers;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

public class EstablishmentsPage extends BasePage{

    public static final String URL = BASE_URL + "/establishments";



    public EstablishmentsPage(WebDriver driver) {
        super(driver);
    }



    public EstablishmentsPage isCurrentPage(){
        isCurrentPage(URL);
        return this;
    }

}
