package com.thoughtworks.sandybums.mallworld.helpers;


import org.openqa.selenium.WebDriver;

public class CalendarPage extends BasePage {

    public static final String URL = BASE_URL + "/calendar";

    public CalendarPage(WebDriver driver) {
        super(driver);
    }

    public CalendarPage isCurrentPage() {
        isCurrentPage(URL);
        return this;
    }
}
