package com.thoughtworks.sandybums.mallworld.helpers;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;
import java.util.NoSuchElementException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public abstract class BasePage {

    public static final String BASE_URL = "http://localhost:8080";
    private final static int MAX_WAIT = 30;
    protected static WebDriver driver;
    private final String mainPageHandle;
    private final By hamburgerButton = By.id("hamburgerButton");
    private final By sidebarButton = By.id("sidebarButton");
    private final By adminSideBarButton = By.id("adminSidebarButton");

    public BasePage(WebDriver driver) {
        this.driver = driver;
        mainPageHandle = driver.getWindowHandle();
    }

    protected WebElement findAdminSideBarElement(By by) {
        openMenuFor(by, adminSideBarButton);
        return findElement(by);
    }

    protected WebElement findHeaderNavBarElement(By by) {
        openMenuFor(by, hamburgerButton);
        return findElement(by);
    }

    protected WebElement findSideBarElement(By by) {
        openMenuFor(by, sidebarButton);
        return findElement(by);
    }

    private void openMenuFor(By by, By navbarButton) {
        if (!findElement(by).isDisplayed()) {
            findElement(navbarButton).click();
            waitForElementToDisplay(by);
        }
    }

    public void waitForElement(By by) {
        int counter = 0;
        while (counter++ <= MAX_WAIT) {
            try {
                findElement(by);
                break;
            } catch (NoSuchElementException e) {
                sleep(1);
            }
        }
    }

    protected void waitForElementToDisplay(By by) {
        int counter = 0;
        while (counter++ <= MAX_WAIT && !findElement(by).isDisplayed()) {
            sleep(1);
        }
    }

    public WebElement findElement(By by) {
        return driver.findElement(by);
    }

    public List<WebElement> findElements(By by) {
        return driver.findElements(by);
    }

    public boolean elementExists(By by) {
        try {
            findElement(by);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    protected void sleep(long timeInSeconds) {
        try {
            Thread.sleep(timeInSeconds * 1000);
        } catch (Exception e) {
            // Eat
        }
    }

    public void switchBackToMainWindow() {
        driver.switchTo().window(mainPageHandle);
    }

    public void isCurrentPage(String url) {
        assertThat(driver.getCurrentUrl().split("\\?")[0], is(url));
    }

    protected void fillField(String text, By by) {
        WebElement emailElement = findElement(by);
        emailElement.clear();
        emailElement.sendKeys(text);
    }


}
