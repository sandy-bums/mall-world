package com.thoughtworks.sandybums.mallworld;


import org.junit.Test;

public class HomeJourneyTest extends SeleniumTest {

    @Test
    public void shouldGoToIndexPage() {
        homepage
                .visit()
                .isCurrentPage();
    }

    @Test
    public void shouldVisitLinkedPages() {
        homepage
                .visit()
                .isCurrentPage()
                .clickButton("calendarLink");

        calendarPage
                .isCurrentPage();

        homepage
                .visit()
                .isCurrentPage()
                .clickButton("eventsLink");

        eventsPage
                .isCurrentPage();

        homepage
                .visit()
                .isCurrentPage()
                .clickButton("establishmentsLink");

        establishmentsPage
                .isCurrentPage();
    }
}
