package com.thoughtworks.sandybums.mallworld;

import org.junit.Test;

public class AdminJourneyTest extends SeleniumTest {

    @Test
    public void adminJourney() {
        homepage
                .visit();

        header
                .clickLogIn();

        logInPage
                .isCurrentPage()
                .fillForm("venue_user@venue.com", "password")
                .submit();

        header
                .displaysAdminButton()
                .clickAdminButton();

        adminPage
                .isCurrentPage()
                .displaysManageEstablishmentsButton()
                .displaysAddEstablishmentButton()
                .displaysManageEventsButton()
                .displaysAddEventButton();

        adminPage
                .clickAddEventButton();

        addEventPage
                .isCurrentPage()
                .showsUploadImageButton();

        adminPage
                .visit()
                .clickManageEstabLink();

        establishmentsManagePage
                .isCurrentPage();

        header
                .clickLogout();
    }
}
