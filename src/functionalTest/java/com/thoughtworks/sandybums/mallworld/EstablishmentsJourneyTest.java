package com.thoughtworks.sandybums.mallworld;


import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class EstablishmentsJourneyTest extends SeleniumTest{
    @Test
    public void shouldGoToAddEstablishmentsPage(){
        //only a venue user can access this page
        logInPage.visit()
                 .fillForm("venue_user@venue.com", "password")
                 .submit();

        addEstablishmentsPage.visit();
        String pageTitle = driver.getTitle();
        assertThat(pageTitle, is("Mall World - Add Establishment"));
    }

    @Test
    public void establishmentJourney(){
        //happy path
        addEstablishmentsPage
                .visit()
                .defaultFill("A title", "same1@email.com", "password")
                .submit();

        establishmentsPage
                .isCurrentPage();

        //sad path
        addEstablishmentsPage
                .visit()
                .defaultFill("A title", "same1@email.com", "password")
                .submit()
                .showUserEmailMessage("Email already in use");

    }
}
