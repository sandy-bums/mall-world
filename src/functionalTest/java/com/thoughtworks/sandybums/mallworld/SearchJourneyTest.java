package com.thoughtworks.sandybums.mallworld;

import org.junit.Test;

public class SearchJourneyTest extends SeleniumTest {

    @Test
    public void searchJourney() {

        String title = "Search Journey Test";

        //only a venue user can access add event page
        logInPage.visit()
                .fillForm("venue_user@venue.com", "password")
                .submit();

        addEventPage
                .visit()
                .defaultFill(title)
                .saveEvent();

        header
                .entersTextIntoSearchBox("Search")
                .clickSearchSubmit();

        eventsPage
                .lastEventIs(title);

        header
                .entersTextIntoSearchBox("")
                .clickSearchSubmit();

        eventsPage
                .isCurrentPage()
                .lastEventIs(title);
        eventsManagePage
                .visit()
                .deleteLastAddedEvent();
        eventsPage
                .visit()
                .lastEventIsNot(title);
    }
}
