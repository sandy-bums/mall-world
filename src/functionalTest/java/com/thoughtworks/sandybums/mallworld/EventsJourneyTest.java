package com.thoughtworks.sandybums.mallworld;

import org.joda.time.LocalDateTime;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;


public class EventsJourneyTest extends SeleniumTest {

    private String title = "Awesome Functional Title";
    private LocalDateTime startDateTime = LocalDateTime.now().plusDays(1).withTime(11, 34, 0, 0);
    private LocalDateTime endDateTime = LocalDateTime.now().plusDays(2).plusHours(1).withTime(11, 34, 0, 0);
    private LocalDateTime badEndDateTime = LocalDateTime.now().plusHours(1).withTime(11, 34, 0, 0);

    private LocalDateTime filterStartDateTime = startDateTime;
    private LocalDateTime filterEndDateTime = endDateTime;

    private String startDateString = "" + startDateTime.getYear() + " " + startDateTime.getHourOfDay();
    private String endDateString = "" + endDateTime.getYear() + " " + endDateTime.getHourOfDay();
    private String longTitle = "Lorem ipsum dolor sit amet, consectetur adipiscing elit." +
            " Vivamus posuere placerat mi id tempor. Duis id semper est. Aliquam dapibus, mauris " +
            "eu pulvinar convallis, elit elit volutpat tellus, non malesuada tortor orci aliquet velit." +
            " Etiam libero massa nunc258.";

    private String categoryString = "Fun & Games";

    @Test
    public void shouldGoToAddEventsPage() {
        //only a venue user can access this page
        logInPage.visit()
                .fillForm("venue_user@venue.com", "password")
                .submit();


        addEventPage.visit();

        String pageTitle = driver.getTitle();

        assertThat(pageTitle, is("Mall World - Add Event"));
    }

    @Test
    public void eventJourney() {
        //only a venue user can access the add event page page
        logInPage.visit()
                .fillForm("venue_user@venue.com", "password")
                .submit();

        // sad path
        addEventPage
                .visit()
                .defaultFill(title, startDateTime, badEndDateTime, categoryString)
                .saveEvent()
                .showsDateErrorMessage("Event end date / time must be after event start")
                .defaultFill(longTitle, startDateTime, endDateTime, categoryString)
                .saveEvent()
                .showsTitleErrorMessage("Title is too long, must be less than 255 characters");


        // happy path
        addEventPage
                .visit()
                .defaultFill(title, startDateTime, endDateTime, categoryString)
                .saveEvent();

        adminPage
                .isCurrentPage();

        eventsManagePage
                .visit()
                .lastEventIs(title);

        eventsPage
                .visit()
                .lastEventIs(title)
                .hasDatePresent(startDateString, endDateString)
                .hasOwnerPresent("Mallworld")
                .hasColour("#fff277") //colour specific to Fun & Games category
                .renderEventsDetails(title)
                .hasCategoryPresent();

        // test filter functionality
        //by category
        eventsPage
                 .visit()
                 .selectCategoryFilters(categoryString) // select filter option
                 .lastEventIs(title);

        eventsPage.visit()
                  .selectCategoryFilters("Exhibition")
                  .lastEventIsNot(title);
        //by date
        eventsPage
                .visit()
                .filterByDate(filterStartDateTime, filterEndDateTime)
                .lastEventIs(title);
        eventsPage
                .visit()
                .filterByDate(filterEndDateTime.plusDays(3),filterEndDateTime.plusDays(4))
                .lastEventIsNot(title);

        //make sure year of date is truncated when entered in the filter
        eventsPage
                .visit()
                .filterByDate(filterStartDateTime.withYear(10000),filterEndDateTime.withYear(10000))
                .thereIsNo500Error();

        // try to visit My Events page without being logged in
        eventsPage.visit();
        header.clickLogout();

        // set up sample user
        signUpPage
                .visit()
                .fillForm("user@email.com", "password")
                .submit();
        header
                .clickLogout();

        myEventsPage
                .visit();

        logInPage
                .isCurrentPage()
                .fillForm("user@email.com", "password")
                .submit();

        myEventsPage
                .isCurrentPage();

        eventsPage
                .visit()
                .lastEventIs(title)
                .attendFirstEvent();

        myEventsPage
                .visit()
                .isCurrentPage()
                .lastEventIs(title)
                .hasColour("#fff277") //colour specific to Fun & Games category
                .clickLeaveEventButton(title)
                .isCurrentPage()
                .lastEventIsNot(title);

        // test that events can be deleted (by a venue/establishment user)
        header.clickLogout();
        logInPage.visit()
                .fillForm("venue_user@venue.com", "password")
                .submit();

        eventsManagePage
                .visit()
                .lastEventIs(title)
                .deleteLastAddedEvent()
                .lastEventIsNot(title);

        eventsPage
                .visit()
                .lastEventIsNot(title);

    }

}