package com.thoughtworks.sandybums.mallworld;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.thoughtworks.sandybums.mallworld.helpers.*;
import com.thoughtworks.sandybums.mallworld.web.config.DatabaseConfig;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

@DatabaseSetup("classpath:db/test-data.xml")
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DatabaseConfig.class})
@TestExecutionListeners({
        DependencyInjectionTestExecutionListener.class,
        DbUnitTestExecutionListener.class})
public abstract class SeleniumTest {

    protected static WebDriver driver;

    protected static Homepage homepage;
    protected static Header header;
    protected static SignUpPage signUpPage;
    protected static LogInPage logInPage;
    protected static EventsPage eventsPage;
    protected static MyEventsPage myEventsPage;
    protected static AddEventsPage addEventPage;
    protected static AddEstablishmentsPage addEstablishmentsPage;
    protected static EstablishmentsManagePage establishmentsManagePage;
    protected static EstablishmentsPage establishmentsPage;
    protected static CalendarPage calendarPage;
    protected static EventsManagePage eventsManagePage;
    protected static AdminPage adminPage;

    @BeforeClass
    public static void before() {
        driver = new HtmlUnitDriver(true);
        homepage = new Homepage(driver);
        header = new Header(driver);
        signUpPage = new SignUpPage(driver);
        logInPage = new LogInPage(driver);
        eventsPage = new EventsPage(driver);
        myEventsPage = new MyEventsPage(driver);
        addEventPage = new AddEventsPage(driver);
        addEstablishmentsPage = new AddEstablishmentsPage(driver);
        establishmentsPage = new EstablishmentsPage(driver);
        calendarPage = new CalendarPage(driver);
        eventsManagePage = new EventsManagePage(driver);
        establishmentsManagePage = new EstablishmentsManagePage(driver);
        adminPage =new AdminPage(driver);
    }

    @AfterClass
    public static void after() {
        driver.close();
    }

}
