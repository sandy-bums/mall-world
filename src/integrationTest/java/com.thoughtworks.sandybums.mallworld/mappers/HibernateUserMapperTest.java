package com.thoughtworks.sandybums.mallworld.mappers;

import com.thoughtworks.sandybums.mallworld.domain.*;
import org.joda.time.LocalDateTime;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;

public class HibernateUserMapperTest extends HibernateMapperBaseTest {

    @Autowired
    private UserMapper userMapper;
    @Autowired
    private EventMapper eventMapper;
    @Autowired
    private AddEventToUserMapper addEventToUserMapper;

    private User user;
    private Event eventYoga;
    private EventCategory eventCategory;
    private Establishment establishment;
    private UserEvents userEvents;


    //TODO: Change to an actual encryptedPassword
    @Before
    public void setUp() throws Exception {
        user = new User("email@example.com", "password", Role.VISITOR_ROLE);
        eventCategory = new EventCategory((short) 1, "Fun & Games", "#fff277");
        establishment = new Establishment("Zen", new User("email", "password", Role.VISITOR_ROLE));
        eventYoga = new Event.EventBuilder("yoga session", LocalDateTime.now(), LocalDateTime.now(), eventCategory, establishment).build();
    }

    @Test
    public void shouldSaveAUser() throws Exception {
        userMapper.saveUser(user);
        assertThat(user.getId(), is(notNullValue()));
    }

    @Test
    public void shouldRetrieveUserByEmail() {
        userMapper.saveUser(user);
        assertThat(userMapper.getUser(user.getEmail()), is(user));
    }
}
