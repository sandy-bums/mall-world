package com.thoughtworks.sandybums.mallworld.mappers;


import com.thoughtworks.sandybums.mallworld.domain.EventCategory;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class HibernateEventCategoryMapperTest extends HibernateMapperBaseTest {

    @Autowired
    private EventCategoryMapper eventCategoryMapper;

    private List<EventCategory> eventCategoryList;

    @Before
    public void setUp() throws Exception {
        eventCategoryList = eventCategoryMapper.getDefaultCategories();

    }

    //TODO: Refactor assertion to check for actual objects
    @Test
    public void shouldReturnTheListOfCategories() {
        assertThat(eventCategoryList.size(), is(7));
    }

    @Test
    public void shouldReturnColourForCategory() {
        EventCategory eventCategory = eventCategoryList.get(0);

        assertThat(eventCategory.getColour(), is("#fff277"));
    }
}
