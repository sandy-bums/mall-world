package com.thoughtworks.sandybums.mallworld.mappers;

import com.thoughtworks.sandybums.mallworld.domain.*;
import org.hibernate.SessionFactory;
import org.joda.time.LocalDateTime;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Set;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.core.Is.is;


public class HibernateAddEventToUserMapperTest extends HibernateMapperBaseTest {

    @Autowired
    private EventMapper eventMapper;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private AddEventToUserMapper addEventToUserMapper;

    private Event eventSale;

    private Event eventYoga;

    private User user;

    private User otherUser;

    private EventCategory eventCategory = new EventCategory((short) 1, "Fun & Games", "#A00000");
    private Establishment establishment = new Establishment("Zen", new User("email", "password", Role.VISITOR_ROLE));
    private UserEvents userEvents;
    private UserEvents otherUserEvents;

    @Before
    public void setup() {
        eventSale = new Event.EventBuilder("mega sale", LocalDateTime.now(), LocalDateTime.now(), eventCategory, establishment).build();
        eventYoga = new Event.EventBuilder("yoga session", LocalDateTime.now(), LocalDateTime.now(), eventCategory, establishment).build();
        user = new User("test1@email.com", "password", Role.VISITOR_ROLE);
        otherUser = new User("otherUser@email.com", "pa$$word", Role.VISITOR_ROLE);
    }

    @Test
    public void shouldReturnEmptyList() {
        assertThat(addEventToUserMapper.getUserEventsById(0L), hasSize(0));
    }

    @Test
    public void shouldOnlyReturnLoggedInUserEvents() {

        eventMapper.saveEvent(eventSale);
        eventMapper.saveEvent(eventYoga);
        userMapper.saveUser(user);
        userMapper.saveUser(otherUser);

        userEvents = new UserEvents(user.getId(), eventSale.getId());
        otherUserEvents = new UserEvents(otherUser.getId(), eventYoga.getId());

        addEventToUserMapper.saveEventToUser(userEvents);
        addEventToUserMapper.saveEventToUser(otherUserEvents);

        List<UserEvents> events = addEventToUserMapper.getUserEventsById(user.getId());
        Long eventId = events.get(0).getUserEvent();
        Event event = eventMapper.getEvent(eventId);

        assertThat(addEventToUserMapper.getUserEventsById(user.getId()), hasSize(1));
        assertThat(events.size(), is(1));
        assertThat(eventId, is(eventSale.getId()));
        assertThat(event.getTitle(), is("mega sale"));

        Set<Event> eventsFromDb = userMapper.getUserWithEvents(user.getEmail()).getAttendedEvents();

        assertThat(eventsFromDb, hasSize(1));
    }


    @Test
    public void simpleTest() {
        eventMapper.saveEvent(eventYoga);


        userMapper.saveUser(user);

//        user.attendEvent(eventYoga);

        eventYoga.addAttendee(user);

//        userMapper.updateUser(user);

        eventMapper.update(eventYoga);

        assertThat(userMapper.getUserWithEvents(user.getEmail()).getAttendedEvents(), hasSize(1));

        eventMapper.removeEvent(eventYoga);
        assertThat(userMapper.getUserWithEvents(user.getEmail()).getAttendedEvents(), hasSize(0));
    }


}
