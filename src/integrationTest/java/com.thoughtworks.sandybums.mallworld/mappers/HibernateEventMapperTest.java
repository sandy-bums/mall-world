package com.thoughtworks.sandybums.mallworld.mappers;

import com.thoughtworks.sandybums.mallworld.domain.*;
import com.thoughtworks.sandybums.mallworld.web.config.ServiceConfig;
import org.hibernate.SessionFactory;
import org.joda.time.LocalDateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.transaction.Transactional;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;

public class HibernateEventMapperTest extends HibernateMapperBaseTest {

    @Autowired
    private EventMapper eventMapper;

    private EventCategory eventCategory = new EventCategory((short) 1, "Fun & Games", "#fff277");
    private Establishment establishment = new Establishment("Zen", new User("email", "password", Role.VISITOR_ROLE));

    @Test
    public void shouldReturnEmptyList() {
        assertThat(eventMapper.getEvents(), hasSize(0));
    }

    @Test
    public void shouldReturnEvents() {
        Event eventSale = new Event.EventBuilder("mega sale", LocalDateTime.now(), LocalDateTime.now(), eventCategory, establishment).build();
        eventMapper.saveEvent(eventSale);

        assertThat(eventMapper.getEvents(), contains(eventSale));
    }

    @Test
    public void shouldNotReturnEventsWhoseEndDateHasPassed() {
        Event eventThatIsPassed = new Event.EventBuilder("passed event",
                LocalDateTime.now().minusDays(2),
                LocalDateTime.now().minusDays(1),
                eventCategory,
                establishment)
                .build();
        Event eventInFuture = new Event.EventBuilder("passed event",
                LocalDateTime.now(),
                LocalDateTime.now().plusMinutes(1),
                eventCategory,
                establishment)
                .build();

        eventMapper.saveEvent(eventThatIsPassed);
        eventMapper.saveEvent(eventInFuture);

        List<Event> listOfEvents = eventMapper.getOngoingAndFutureEvents();

        assertThat(listOfEvents.size(), is(1));

    }

    @Test
    public void shouldSaveAnEvent() {
        Event event = new Event.EventBuilder("mega sale", LocalDateTime.now(), LocalDateTime.now(), eventCategory, establishment).build();
        eventMapper.saveEvent(event);
        assertThat(event.getId(), is(notNullValue()));
    }
    
    @Test
    public void shouldDeleteAnEvent() {
        Event eventSale = new Event.EventBuilder("mega sale", LocalDateTime.now(), LocalDateTime.now(), eventCategory, establishment).build();
        eventMapper.saveEvent(eventSale);

        eventMapper.removeEvent(eventSale.getId());

        assertFalse(eventMapper.getEvents().contains(eventSale));
    }
}
