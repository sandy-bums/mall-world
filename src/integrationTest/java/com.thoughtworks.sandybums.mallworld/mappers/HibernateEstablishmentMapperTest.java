package com.thoughtworks.sandybums.mallworld.mappers;

import com.thoughtworks.sandybums.mallworld.domain.*;
import org.joda.time.LocalDateTime;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Set;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.core.Is.is;

public class HibernateEstablishmentMapperTest extends HibernateMapperBaseTest {

    private User user = new User("email@twu42.com", "password", Role.VISITOR_ROLE);
    private Establishment establishment = new Establishment("Test1 Establishment", user);

    @Autowired
    private EstablishmentMapper establishmentMapper;
    @Autowired
    private EstablishmentEventsMapper establishmentEventsMapper;
    @Autowired
    private EventMapper eventMapper;

    private EventCategory eventCategory = new EventCategory((short) 1, "Fun & Games", "#A00000");
    private Event eventSale;
    private EstablishmentEvents establishmentEvents;

    @Test
    public void shouldSaveEstablishment(){
        establishmentMapper.saveEstablishment(establishment);
        assertThat(establishment.getId(), is(notNullValue()));
    }

    @Test
    public void shouldReturnOwnedEventsForEstablishment() throws Exception {
        eventSale = new Event.EventBuilder("mega sale", LocalDateTime.now(), LocalDateTime.now(), eventCategory, establishment).build();
        eventMapper.saveEvent(eventSale);
        establishmentMapper.saveEstablishment(establishment);
        establishmentEvents = new EstablishmentEvents(establishment.getId(), eventSale.getId());
        establishmentEventsMapper.saveEventToEstablishment(establishmentEvents);

        Set<Event> ownedEvents = establishmentMapper.getOwnedEventsForEstablishment(establishment);
        assertThat(ownedEvents, contains(eventSale));

        establishmentEventsMapper.removeEstablishmentEvents(establishmentEvents);
    }
}

