package com.thoughtworks.sandybums.mallworld.domain;

import org.joda.time.LocalDateTime;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class EventTest {

    // TODO:
    // The community edition of tomcat does not support adding a webserver to IDEA.
    // Therefore this class cannot run, as it has a dependency 'javax.el.ExpressionFactory'
    // See: http://stackoverflow.com/questions/8487048/java-lang-linkageerror-javax-servlet-jsp-jspapplicationcontext-getexpressionfac

//    private static Validator validator;
//
//    @BeforeClass
//    public static void setUp() {
//        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
//        validator = factory.getValidator();
//    }
//
//    @Test
//    public void shouldNotAllowEventsWithEndBeforeStart() throws Exception {
//        Event.EventBuilder event = new Event.EventBuilder()
//                .setCategory("category")
//                .setTitle("title")
//                .setStartDate(LocalDateTime.now())
//                .setEndDate(LocalDateTime.now().minusMinutes(1))
//                .setSpecialInstructions("instructions")
//                .setDescription("description")
//                .setEstablishment(new Establishment("ThoughtWorks", null));
//
//        Set<ConstraintViolation<Event.EventBuilder>> constraintViolations =
//                validator.validate(event);
//
//        assertThat(constraintViolations.size(), is(1));
//        assertThat(constraintViolations.iterator().next().getMessage(),
//                is("Event end date / time must be after event start"));
//    }
}
