package com.thoughtworks.sandybums.mallworld.domain;

import org.junit.Test;
import org.springframework.security.core.GrantedAuthority;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.core.Is.is;

public class UserTest {

    @Test
    public void shouldReturnGrantedAuthoritiesBasedOnRole() throws Exception {
        User user = new User("email@address.com", "password", Role.VISITOR_ROLE);

        List<? extends GrantedAuthority> authorityCollection = user.getGrantedAuthority();

        assertThat(authorityCollection, hasSize(1));
        assertThat(authorityCollection.get(0).getAuthority(), is(Role.VISITOR_ROLE.getValue()));
    }
}
