package com.thoughtworks.sandybums.mallworld.service;

import com.thoughtworks.sandybums.mallworld.domain.Establishment;
import com.thoughtworks.sandybums.mallworld.domain.Event;
import com.thoughtworks.sandybums.mallworld.mappers.HibernateEstablishmentMapper;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import java.util.List;
import java.util.Set;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class EstablishmentServiceTest {
    private EstablishmentService establishmentService;

    @Mock
    private HibernateEstablishmentMapper establishmentMapper;

    @Mock
    private Establishment establishment;

    @Mock
    private List<Establishment> establishments;

    @Mock
    private Set<Event> ownedEvents;

    @Before
    public void setUp(){
        initMocks(this);
        establishmentService = new EstablishmentService(establishmentMapper);
    }

    @Test
    public void shouldSaveOwner() {
        establishmentService.saveOwner(establishment);
        verify(establishmentMapper).saveEstablishment(establishment);
    }

    @Test
    public void shouldRetrieveEstablishments() {
        when(establishmentMapper.getEstablishments()).thenReturn(establishments);
        assertThat(establishmentService.getEstablishments(),is(establishments));
    }

    @Test
    public void shouldReturnOwnedEventsForEstablishment() throws Exception {
        when(establishmentMapper.getOwnedEventsForEstablishment(establishment)).thenReturn(ownedEvents);
        Set<Event> returnedEvents = establishmentService.getOwnedEventsForEstablishment(establishment);

        verify(establishmentMapper).getOwnedEventsForEstablishment(establishment);
        assertThat(returnedEvents, is(ownedEvents));
    }
}
