package com.thoughtworks.sandybums.mallworld.service;

import com.thoughtworks.sandybums.mallworld.domain.Event;
import com.thoughtworks.sandybums.mallworld.mappers.AddEventToUserMapper;
import com.thoughtworks.sandybums.mallworld.mappers.EventMapper;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

public class AddEventsToUserServiceTest {

    private AddEventToUserService addEventToUserService;

    @Mock
    private AddEventToUserMapper addEventToUserMapper;
    @Mock
    private List events;

    @Before
    public void setUp() {
        initMocks(this);
        addEventToUserService = new AddEventToUserService(addEventToUserMapper);
    }

    @Test
    public void shouldReturnListOfEvents(){
        when(addEventToUserMapper.getUserEventsById(0L)).thenReturn(events);
        assertThat(addEventToUserService.getUserEvents(0L), is(events));
    }
}
