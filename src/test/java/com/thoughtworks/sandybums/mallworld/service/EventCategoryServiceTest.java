package com.thoughtworks.sandybums.mallworld.service;

import com.thoughtworks.sandybums.mallworld.domain.EventCategory;
import com.thoughtworks.sandybums.mallworld.mappers.EventCategoryMapper;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.Is.isA;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class EventCategoryServiceTest {

    @Mock
    private EventCategoryMapper eventCategoryMapper;
    @Mock
    private EventCategory eventCategory;

    private EventCategoryService eventCategoryService;

    @Before
    public void setup() {
        initMocks(this);
        eventCategoryService = new EventCategoryService(eventCategoryMapper);
    }

    @Test
    public void shouldRetrieveCategories() {
        eventCategoryService.getDefaultCategories();
        verify(eventCategoryMapper).getDefaultCategories();

        List<EventCategory> aList = new ArrayList<>();
        when(eventCategoryMapper.getDefaultCategories()).thenReturn(aList);
        assertThat(eventCategoryService.getDefaultCategories(), is(aList));
    }

    @Test
    public void shouldReturnCategoryByName() throws Exception {
        when(eventCategoryMapper.getCategoryByName(anyString())).thenReturn(eventCategory);

        EventCategory eventCategoryTest = eventCategoryService.getCategoryByName(anyString());

        verify(eventCategoryMapper).getCategoryByName(anyString());
        assertThat(eventCategoryTest, isA(EventCategory.class));
    }
}
