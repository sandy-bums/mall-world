package com.thoughtworks.sandybums.mallworld.service;


import com.thoughtworks.sandybums.mallworld.domain.Event;
import com.thoughtworks.sandybums.mallworld.mappers.AddEventToUserMapper;
import com.thoughtworks.sandybums.mallworld.mappers.EventMapper;
import com.thoughtworks.sandybums.mallworld.mappers.HibernateUserMapper;
import com.thoughtworks.sandybums.mallworld.mappers.UserMapper;
import org.hibernate.SessionFactory;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Set;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

public class UserServiceTest {

    @Mock
    private UserMapper userMapper;
    @Mock
    private EventMapper eventMapper;
    @Mock
    private AddEventToUserMapper addEventToUserMapper;

    private UserService userService;


    @Before
    public void setUp() throws Exception {
        userService = new UserService(userMapper, eventMapper, addEventToUserMapper);
    }

    @Test
    public void shouldReturnNoEventsWhenUserIsNull() throws Exception {
        Set<Event> myEvents = userService.getUserEvents(null);

        assertThat(myEvents, hasSize(0));
    }
}
