package com.thoughtworks.sandybums.mallworld.service;

import com.thoughtworks.sandybums.mallworld.domain.Event;
import com.thoughtworks.sandybums.mallworld.domain.EventCategory;
import com.thoughtworks.sandybums.mallworld.mappers.EventMapper;
import com.thoughtworks.sandybums.mallworld.web.form.EventFiltersForm;
import org.joda.time.LocalDateTime;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

public class EventServiceTest {

    private static final String SEARCH_TERM_250_CHARS =
            "123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890" +
                    "123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890" +
                    "1234567890123456789012345678901234567890123456789012345678901234567890";
    private EventService eventService;
    @Mock
    private EventMapper eventMapper;
    @Mock
    private List events;
    @Mock
    private Event event;
    @Mock
    private EventFiltersForm eventFiltersForm;

    @Before
    public void setUp() {
        initMocks(this);
        eventService = new EventService(eventMapper);
    }

    @Test
    public void shouldReturnListOfEvents() {
        when(eventMapper.getEvents()).thenReturn(events);
        assertThat(eventService.getEvents(), is(events));
    }

    @Test
    public void shouldReturnListOfOngoingAndFutureEvents() {
        eventService.getOngoingAndFutureEvents();
        verify(eventMapper).getOngoingAndFutureEvents();
    }

    @Test
    public void shouldPerformSearchWithMatchingQuery() {
        List<Event> eventsList = new ArrayList<>();
        eventsList.add(event);
        String testSearchQuery = "title";
        when(event.toString()).thenReturn("eventtitle");

        List searchResult = eventService.applySearchQuery(testSearchQuery, eventsList);

        assertThat(searchResult, is(eventsList));

    }

    @Test
    public void shouldPerformSearchWithNonMatchingQuery() {
        List<Event> eventsList = new ArrayList<>();
        eventsList.add(event);
        String testSearchQuery = "title";
        when(event.toString()).thenReturn("nothing is matching");

        List searchResult = eventService.applySearchQuery(testSearchQuery, eventsList);

        assertThat(searchResult.size(), is(0));

    }

    @Test
    public void shouldNotPerformSearchWheQueryIsLongerThan250Chars() throws Exception {
        List<Event> eventsList = new ArrayList<>();
        eventsList.add(event);

        List searchResult = eventService.applySearchQuery(SEARCH_TERM_250_CHARS.concat("1"), eventsList);

        assertThat(searchResult, is(eventsList));
    }

    @Test
    public void shouldNotPerformSearchWhenQueryIsNull() {
        List<Event> eventsList = new ArrayList<>();
        eventsList.add(event);

        List searchResult = eventService.applySearchQuery(null, eventsList);

        assertThat(searchResult, is(eventsList));

    }

    @Test
    public void shouldNotPerformSearchWhenQueryIsEmpty() {
        List<Event> eventsList = new ArrayList<>();
        eventsList.add(event);

        List searchResult = eventService.applySearchQuery("", eventsList);

        assertThat(searchResult, is(eventsList));

    }

    @Test
    public void shouldApplyMatchingCategoryFilter() {
        List<Event> eventsList = new ArrayList<>();
        eventsList.add(event);
        List<String> selectedEventCategory = new ArrayList<>();
        selectedEventCategory.add("matching category");

        when(event.getEventCategory()).thenReturn(mock(EventCategory.class));
        when(event.getEventCategory().getName()).thenReturn("matching category");

        List filterResult = eventService.applyCategoryFilter(selectedEventCategory, eventsList);

        assertThat(filterResult, is(eventsList));
    }

    @Test
    public void shouldApplyNonMatchingCategoryFilter() {
        List<Event> eventsList = new ArrayList<>();
        eventsList.add(event);
        List<String> selectedEventCategory = new ArrayList<>();
        selectedEventCategory.add("any category");

        when(event.getEventCategory()).thenReturn(mock(EventCategory.class));
        when(event.getEventCategory().getName()).thenReturn("no match");

        List filterResult = eventService.applyCategoryFilter(selectedEventCategory, eventsList);

        assertThat(filterResult.size(), is(0));
    }

    @Test
    public void shouldNotApplyCategoryFilterWhenSelectedListIsNull() {
        List<Event> eventsList = new ArrayList<>();
        eventsList.add(event);
        List<String> selectedEventCategory = null;

        List filterResult = eventService.applyCategoryFilter(selectedEventCategory, eventsList);

        assertThat(filterResult, is(eventsList));

    }

    @Test
    public void shouldApplyDateFilterWhenOneFilterDateIsBetweenEventsDates() {
        LocalDateTime filterStartDate = new LocalDateTime().withDate(2015,6,1).withTime(12,0,0,0);
        LocalDateTime filterEndDate = new LocalDateTime().withDate(2015,6,6).withTime(12,0,0,0);

        when(event.getStartTimeDate()).thenReturn( new LocalDateTime().withDate(2015,6,2).withTime(12,0,0,0));
        when(event.getEndTimeDate()).thenReturn(new LocalDateTime().withDate(2015, 6, 7).withTime(12, 0, 0, 0));

        List<Event> eventsList = new ArrayList<>();
        eventsList.add(event);

        List filterResult = eventService.applyDateFilter(filterStartDate, filterEndDate, eventsList);

        assertThat(filterResult, is(eventsList));
    }

    @Test
    public void shouldApplyDateFilterWhenEventsDatesAreBetweenFilterDates() {
        LocalDateTime filterStartDate = new LocalDateTime().withDate(2015,6,1).withTime(12,0,0,0);
        LocalDateTime filterEndDate = new LocalDateTime().withDate(2015,6,6).withTime(12,0,0,0);

        when(event.getStartTimeDate()).thenReturn( new LocalDateTime().withDate(2015,6,2).withTime(12,0,0,0));
        when(event.getEndTimeDate()).thenReturn( new LocalDateTime().withDate(2015,6,5).withTime(12,0,0,0));

        List<Event> eventsList = new ArrayList<>();
        eventsList.add(event);

        List filterResult = eventService.applyDateFilter(filterStartDate,filterEndDate,eventsList);

        assertThat(filterResult, is(eventsList));
    }

    @Test
    public void shouldApplyDateFilterWhenFilterDatesAreBetweenEventDates() {
        LocalDateTime filterStartDate = new LocalDateTime().withDate(2015,6,1).withTime(12,0,0,0);
        LocalDateTime filterEndDate = new LocalDateTime().withDate(2015,6,6).withTime(12,0,0,0);

        when(event.getStartTimeDate()).thenReturn( new LocalDateTime().withDate(2015,5,30).withTime(12,0,0,0));
        when(event.getEndTimeDate()).thenReturn( new LocalDateTime().withDate(2015,6,7).withTime(12,0,0,0));

        List<Event> eventsList = new ArrayList<>();
        eventsList.add(event);

        List filterResult = eventService.applyDateFilter(filterStartDate,filterEndDate,eventsList);

        assertThat(filterResult, is(eventsList));
    }

    @Test
    public void shouldNotApplyDateFilterWhenFilterDatesDoNotOverlapEventDates() {
        LocalDateTime filterStartDate = new LocalDateTime().withDate(2015,6,1).withTime(12,0,0,0);
        LocalDateTime filterEndDate = new LocalDateTime().withDate(2015,6,6).withTime(12,0,0,0);

        when(event.getStartTimeDate()).thenReturn( new LocalDateTime().withDate(2015,6,7).withTime(12,0,0,0));
        when(event.getEndTimeDate()).thenReturn( new LocalDateTime().withDate(2015,6,8).withTime(12,0,0,0));

        List<Event> eventsList = new ArrayList<>();
        eventsList.add(event);

        List filterResult = eventService.applyDateFilter(filterStartDate,filterEndDate,eventsList);

        assertThat(filterResult.size(), is(0));
    }

}
