package com.thoughtworks.sandybums.mallworld.web.controller;

import com.thoughtworks.sandybums.mallworld.domain.User;
import com.thoughtworks.sandybums.mallworld.service.EventCategoryService;
import com.thoughtworks.sandybums.mallworld.service.EventService;
import com.thoughtworks.sandybums.mallworld.service.UserContext;
import com.thoughtworks.sandybums.mallworld.service.UserService;
import com.thoughtworks.sandybums.mallworld.web.form.EventFiltersForm;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;

import java.util.ArrayList;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasKey;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class CalendarControllerTest {

    private Model model = new ExtendedModelMap();

    @Mock
    private User user;
    @Mock
    private UserService userService;
    @Mock
    private UserContext userContext;
    @Mock
    private EventService eventService;
    @Mock
    private EventCategoryService eventCategoryService;
    @InjectMocks
    private CalendarController calendarController;


    @Before
    public void setUp() {
        initMocks(this);
        when(eventService.getEvents()).thenReturn(new ArrayList<>());
    }

    @Test
    public void shouldReturnHome() {
        EventFiltersForm eventFiltersForm = mock(EventFiltersForm.class);
        when(eventFiltersForm.getSelectedEventCategories()).thenReturn(null);
        when(userContext.getCurrentUser()).thenReturn(user);
        assertThat(calendarController.index(eventFiltersForm,model), is("calendar"));
    }

    @Test
    public void shouldAddEventsToModel() {
        EventFiltersForm eventFiltersForm = mock(EventFiltersForm.class);
        when(eventFiltersForm.getSelectedEventCategories()).thenReturn(null);
        String view = calendarController.index(eventFiltersForm,model);
        verify(eventService).getEvents();
        assertThat(model.asMap(), hasKey("events"));
    }
}