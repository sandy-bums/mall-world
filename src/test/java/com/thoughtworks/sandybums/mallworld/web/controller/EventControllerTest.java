package com.thoughtworks.sandybums.mallworld.web.controller;

import com.thoughtworks.sandybums.mallworld.domain.*;
import com.thoughtworks.sandybums.mallworld.service.*;
import com.thoughtworks.sandybums.mallworld.web.form.EventFiltersForm;
import com.thoughtworks.sandybums.mallworld.web.form.EventForm;
import com.thoughtworks.sandybums.mallworld.web.form.SelectedDatesForm;
import org.joda.time.LocalDateTime;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributesModelMap;

import javax.servlet.http.HttpServletRequest;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;


public class EventControllerTest {

    private static final String SEARCH_TERM_250_CHARS =
            "123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890" +
                    "123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890" +
                    "1234567890123456789012345678901234567890123456789012345678901234567890";

    @Mock
    private Establishment establishment;
    @Mock
    private EventService eventService;
    @Mock
    private Event event;
    @Mock
    private HttpServletRequest httpRequest;
    @Mock
    private Model model;
    @Mock
    private EventCategoryService eventCategoryService;
    @Mock
    private EventFiltersForm eventFiltersForm;
    @Mock
    private SelectedDatesForm selectedDatesForm;
    @Mock
    private UserContext userContext;
    @Mock
    private EstablishmentService establishmentService;
    @Mock
    private EstablishmentEventsService establishmentEventsService;
    @Mock
    private List<Event> finalList;
    @Mock
    private BindingResult result;
    @InjectMocks
    private EventController eventController;


    @Before
    public void setUp() throws Exception {
        initMocks(this);
    }

    @Test
    public void shouldSaveTheEvent() {
        EventForm event = new EventForm()
                .setCategory("Fun & Games")
                .setTitle("title")
                .setStartDate(LocalDateTime.now())
                .setEndDate(LocalDateTime.now())
                .setSpecialInstructions("instructions")
                .setDescription("description");

        BindingResult result = mock(BindingResult.class);
        Establishment establishment = mock(Establishment.class);
        when(establishment.getId()).thenReturn(1L);

        when(result.hasErrors()).thenReturn(false);
        when(establishmentService.getEstablishment(anyObject())).thenReturn(establishment);

        eventController.saveEvent(event, result, model, null);
        verify(eventCategoryService).getCategoryByName(any(String.class));
        verify(eventService).saveEvent(anyObject());
    }

    @Test
    public void shouldNotSaveTheEventWhenCategoryIsNotSelected() {
        EventForm event = new EventForm()
                .setTitle("title")
                .setStartDate(LocalDateTime.now())
                .setEndDate(LocalDateTime.now())
                .setSpecialInstructions("instructions")
                .setDescription("description");

        BindingResult result = mock(BindingResult.class);
        when(result.hasErrors()).thenReturn(true);

        eventController.saveEvent(event, result, model, null);

        verify(eventService, never()).saveEvent(anyObject());
    }

    @Test
    public void shouldNotSaveTheEventWhenYearHasMoreThanFourDigits() {
        EventForm event = new EventForm()
                .setTitle("title")
                .category("Fun & Games")
                .setStartDate(LocalDateTime.now().withYear(12345))
                .setEndDate(LocalDateTime.now().withYear(12345))
                .setSpecialInstructions("instructions")
                .setDescription("description");

        BindingResult result = mock(BindingResult.class);
        when(result.hasErrors()).thenReturn(true);

        eventController.saveEvent(event, result, model, null);

        verify(eventService, never()).saveEvent(anyObject());
    }

    @Test
    public void shouldDeleteTheEvent() {
        RedirectAttributes redirectAttributes = new RedirectAttributesModelMap();

        eventController.deleteEvent(anyLong(), redirectAttributes);

        verify(eventService).removeEvent(anyLong());
    }

    @Test
    public void shouldPerformMainSearch() {
        eventController.processSearch(httpRequest, model);

        verify(eventService).applySearchQuery(anyString(), anyList());
    }

    @Test
    public void shouldFilterEventsWhenFilterOptionsSelected() {
        when(httpRequest.getParameter("search")).thenReturn(null);
        eventController.index(eventFiltersForm, httpRequest, model,result);

        when(eventCategoryService.getDefaultCategories()).thenReturn(new ArrayList<EventCategory>());
        when(eventFiltersForm.getSelectedEventCategories()).thenReturn(new ArrayList<String>());

        verify(eventCategoryService, atLeastOnce()).getDefaultCategories();
    }

    @Test
    public void shouldNotFilterEventsWhenFilterEndDateIsEarlierThanStart() {
        List eventList = new ArrayList<>();
        LocalDateTime filterStartDate = LocalDateTime.now();
        when(eventFiltersForm.getFilterStartDate()).thenReturn(filterStartDate);
        when(eventFiltersForm.getFilterEndDate()).thenReturn(filterStartDate.minusDays(1));

        eventController.index(eventFiltersForm, httpRequest, model, result);

        verify(eventService, never()).applyFilters(eventFiltersForm,eventList);

    }

    @Test
    public void shouldReturnManageEventsWhenManageEventsSelected() throws Exception {
        assertThat(eventController.manage(model), is("manageEvents"));

        verify(userContext).getCurrentUser();
        verify(establishmentService).getEstablishment(anyObject());
        verify(establishmentService).getOwnedEventsForEstablishment(anyObject());
    }
}