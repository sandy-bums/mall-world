package com.thoughtworks.sandybums.mallworld.web.controller;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

public class HomeControllerTest {

    private HomeController homeController;

    @Before
    public void setUp() throws Exception {
        homeController = new HomeController();
    }

    @Test
    public void shouldReturnIndex() throws Exception {

        assertThat(homeController.index(), is("index"));
    }
}