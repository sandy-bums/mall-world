package com.thoughtworks.sandybums.mallworld.web.controller;

import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class AdminControllerTest {

    private AdminController adminController = new AdminController();

    @Test
    public void shouldReturnAdminInterfacePage() {
        assertThat(adminController.index(), is("admin"));
    }
}