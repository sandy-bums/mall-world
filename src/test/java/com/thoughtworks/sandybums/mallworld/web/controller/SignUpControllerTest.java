package com.thoughtworks.sandybums.mallworld.web.controller;

import com.thoughtworks.sandybums.mallworld.domain.User;
import com.thoughtworks.sandybums.mallworld.service.UserContext;
import com.thoughtworks.sandybums.mallworld.service.UserService;
import com.thoughtworks.sandybums.mallworld.web.form.UserForm;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.validation.BindingResult;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.mockito.internal.verification.VerificationModeFactory.times;

public class SignUpControllerTest {

    private SignUpController signUpController;

    @Mock
    private UserService userService;
    @Mock
    private UserContext userContext;
    @Mock
    private JavaMailSender javaMailSender;
    @Mock
    private UserForm userForm;
    @Mock
    private User user;
    @Mock
    private BindingResult result;

    @Before
    public void setUp() throws Exception {
        initMocks(this);
        signUpController = new SignUpController(userService, userContext, javaMailSender);
    }

    @Test
    public void showsSignUpPage() throws Exception {
        assertThat(signUpController.index(userForm), is("signUp"));
    }

    @Test
    public void shouldSaveUser() {
        when(result.hasErrors()).thenReturn(false);
        when(userForm.build()).thenReturn(user);
        String page = signUpController.signUp(userForm, result);

        verify(userService).saveUser(user);
        verify(userContext).setCurrentUser(user);
        assertThat(page, is("redirect:/"));
    }


    @Test
    public void shouldNotSaveUserWhenUserIsInvalid() {
        when(result.hasErrors()).thenReturn(true);
        when(userForm.build()).thenReturn(user);
        String page = signUpController.signUp(userForm, result);

        verify(userService, times(0)).saveUser(user);
        verifyZeroInteractions(userContext);
        assertThat(page, is("signUp"));
    }

    @Test
    public void shouldSendWelcomeEmailOnSignUp() {
        signUpController.signUp(userForm, result);

        verify(javaMailSender).send(any(MimeMessagePreparator.class));
    }
}
