package com.thoughtworks.sandybums.mallworld.web.controller;

import com.thoughtworks.sandybums.mallworld.domain.Role;
import com.thoughtworks.sandybums.mallworld.domain.User;
import com.thoughtworks.sandybums.mallworld.service.AddEventToUserService;
import com.thoughtworks.sandybums.mallworld.service.EventService;
import com.thoughtworks.sandybums.mallworld.service.UserContext;
import com.thoughtworks.sandybums.mallworld.service.UserService;
import org.apache.commons.io.IOExceptionWithCause;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.web.servlet.ModelAndView;

import java.sql.SQLException;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;


public class MyEventsControllerTest {

    Long eventId = 1L;
    @Mock
    private EventService eventService;
    @Mock
    private UserService userService;
    @Mock
    private UserContext userContext;
    @Mock
    private AddEventToUserService addEventToUserService;
    @InjectMocks
    private MyEventsController myEventsController;

    @Before
    public void setUp() {
        initMocks(this);
    }

    @Test
    public void shouldAddEventToUser() {
        User user = new User("email@email.com", "password", Role.VISITOR_ROLE);
        when(userContext.getCurrentUser()).thenReturn(user);

        myEventsController.addEventToUser(eventId);

        verify(addEventToUserService).addEventToUser(anyObject());
    }

    @Test
    public void shouldRemoveEventFromUserEventsWhenUserLeavesEvent() {

        myEventsController.removeEventFromUser(eventId);

        verify(eventService).removeUserFromEvent(anyLong(), anyObject());
    }


}