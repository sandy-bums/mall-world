package com.thoughtworks.sandybums.mallworld.web.controller;

import com.thoughtworks.sandybums.mallworld.domain.Establishment;
import com.thoughtworks.sandybums.mallworld.domain.User;
import com.thoughtworks.sandybums.mallworld.service.EstablishmentService;
import com.thoughtworks.sandybums.mallworld.service.UserService;
import com.thoughtworks.sandybums.mallworld.web.form.EstablishmentForm;
import com.thoughtworks.sandybums.mallworld.web.form.UserForm;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

public class EstablishmentControllerTest {


    @InjectMocks
    private EstablishmentController establishmentController;
    @Mock
    private EstablishmentService establishmentService;
    @Mock
    private EstablishmentForm establishmentForm;
    @Mock
    private UserForm userForm;
    @Mock
    private User user;
    @Mock
    private Establishment establishment;
    @Mock
    private UserService userService;
    @Mock
    private BindingResult result;
    @Mock
    private Model model;

    @Before
    public void setUp() {
        initMocks(this);
    }

    @Test
    public void shouldReturnAddEstablishment() {
        assertThat(establishmentController.establishment(establishmentForm), is("addEstablishment"));
    }

    @Test
    public void shouldSaveEstablishment() {
        when(establishmentForm.getUserForm()).thenReturn(userForm);
        when(userService.getUser("email@email.com")).thenReturn(null);
        when(result.hasErrors()).thenReturn(false);

        establishmentController.saveEstablishment(establishmentForm, result);

        verify(establishmentService).saveOwner(establishmentForm.build());
    }

    @Test
    public void shouldNotSaveEstablishmentWhenUserEmailAlreadyExists() {
        when(establishmentForm.getUserForm()).thenReturn(userForm);
        when(userService.getUser("email@email.com")).thenReturn(user);
        when(result.hasErrors()).thenReturn(true);

        String page = establishmentController.saveEstablishment(establishmentForm, result);

        verify(userService, times(0)).saveUser(userForm.build());
        verify(establishmentService, times(0)).saveOwner(establishmentForm.build());
        assertThat(page, is("addEstablishment"));
    }

    @Test
    public void shouldReturnManageEstablishments() throws Exception {
        assertThat(establishmentController.manage(model), is("manageEstablishments"));
    }
}
