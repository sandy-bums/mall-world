_It's a Mall World after all_.

## Installation

*Requires*: 
1. Java 8
2. Node Package Manager
3. Grunt JS


## Setup

1. Install Java 8
2. Ensure `java -version` shows `1.8.xx`. (You may need to point `JAVA_HOME` at Java 8)
3. Import the Gradle project in IntelliJ and specify the SDK as Java 8
    1. Open IntelliJ IDEA and close any current projects
    2. Select the "Import Project" option from the welcome window
    3. Select the Mall World project and click Ok
    4. Choose "Import project from external model" and select Gradle then click Next
    5. If available, select 1.8 as Gradle JVM (this can also be changed in the project later)
    6. Click "Finish", done!
4. Install Postgres 9.4 and run it. 
5. Run local build (The first time this may take a while). This will also run the unit, integration and functional tests.
    `./gradlew clean build`
6. To add the venue user run:
    `./gradlew createVenueUser`
7. To populate with sample data run: 
     `psql -h localhost -U postgres --password -d mallworld < db/backup.sql`


## UI

1. Before making changes to the UI run `grunt watch`. This will look for changes in the Less files and compile them automatically to CSS.
2. Only modify Less files, not the CSS ones!
3. Push both Less and CSS files.


## AWS instance management

# In order to run the ansible scripts:

1. Install Ansible - `brew install ansible`. Requires Homebrew.
2. Install Pip - `sudo easy_install pip`.
3. Install Boto - `pip install boto`.
4. See `http://docs.pythonboto.org/en/latest/boto_config_tut.html` for Boto credentials config.


## Credits

+ Cristian Ivascu
+ Gareth Williams
+ Aliki Yiannakou
+ Elizabeth Chesters
+ Matthew Woodruff
+ Matt Weems
+ Leslie I'Anson
+ Raluca Puichilita
