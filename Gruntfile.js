module.exports = function(grunt) {
	grunt.initConfig({
		// running `grunt less` will compile once
		less: {
			development: {
				options: {
					paths: ["./src/main/webapp/resources/less"],
					yuicompress: true
				},
			files: {
				"./src/main/webapp/resources/css/index.css": "./src/main/webapp/resources/less/index.less",
				"./src/main/webapp/resources/css/adminPages.css": "./src/main/webapp/resources/less/adminPages.less"
			}
		}
	},
	// running `grunt watch` will watch for changes
	watch: {
		files: "./src/main/webapp/resources/less/*.less",
		tasks: ["less"]
	}
});
	grunt.loadNpmTasks('grunt-contrib-less');
	grunt.loadNpmTasks('grunt-contrib-watch');
};
