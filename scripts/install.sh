#!/bin/bash

set -e

USER=$1
HOST=$2

if [ $# -lt 2 ]; then
echo usage: scripts/install.sh USER HOST
exit -1
fi;

if [ ! -e "dist/mall-world.zip" ]; then
echo "cannot find dist/mall-world.zip to deploy"
exit -1
fi;

type -p java > /dev/null || (echo "java not found" && exit -1)

scp -i ~/.ssh/sandybums-mallworld-keypair.pem dist/mall-world.zip ${USER}@${HOST}:/tmp

ssh -i ~/.ssh/sandybums-mallworld-keypair.pem ${USER}@${HOST} /bin/bash << EOF

cd /tmp
find . -not -name '*.zip' | xargs rm -rf
unzip mall-world.zip
sh scripts/stop-server.sh
mv mall-world.war ROOT.war
sh db/flyway/flyway migrate
sudo rm -rf /var/lib/tomcat7/webapps/ROOT
sudo cp -f ROOT.war /var/lib/tomcat7/webapps
nohup sh scripts/start-server.sh > server.out 2> server.err < /dev/null

EOF

